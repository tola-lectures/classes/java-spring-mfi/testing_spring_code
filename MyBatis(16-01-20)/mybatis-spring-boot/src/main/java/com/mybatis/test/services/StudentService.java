package com.mybatis.test.services;

import com.mybatis.test.models.Student;

import java.util.ArrayList;

public interface StudentService {

    ArrayList<Student> findAll();
    Student findById(int studentId);
    Student save(Student student);
    boolean delete(int id);




}
