package com.mybatis.test.repositories;

import com.mybatis.test.models.Student;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface StudentRepo {
    @Results(value = {
            @Result(property = "fullName", column = "full_name"),
            //@Result(property = "gender", column = "gender"),
    })
    @Select("SELECT id, full_name, gender, major FROM student")
    ArrayList<Student> findAll();


    @Select("SELECT id, full_name, gender, major FROM student where id = #{id}")
    Student findById(@Param("id") int studentId);

    @Insert("INSERT INTO student values" +
            "(#{stu.id}," +
            " #{stu.fullName}, " +
            " #{stu.gender}," +
            " #{stu.major}" +
            ")")
    Student save(@Param("stu") Student student);

    @Delete("DELETE FROM student WHERE id = #{id}  ")
    boolean delete(int id);




}
