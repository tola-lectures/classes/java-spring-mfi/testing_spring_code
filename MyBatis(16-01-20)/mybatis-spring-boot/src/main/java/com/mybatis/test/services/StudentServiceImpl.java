package com.mybatis.test.services;

import com.mybatis.test.models.Student;
import com.mybatis.test.repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class StudentServiceImpl implements StudentService{

    @Autowired
    private StudentRepo studentRepo;

    @Override
    public ArrayList<Student> findAll() {
        return studentRepo.findAll();
    }

    @Override
    public Student findById(int studentId) {
        return studentRepo.findById(studentId);
    }

    @Override
    public Student save(Student student) {
        return studentRepo.save(student);
    }

    @Override
    public boolean delete(int id) {
        return studentRepo.delete(id);
    }
}
