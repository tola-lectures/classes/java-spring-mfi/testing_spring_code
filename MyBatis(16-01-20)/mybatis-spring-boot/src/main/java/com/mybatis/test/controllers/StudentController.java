package com.mybatis.test.controllers;

import com.mybatis.test.models.Student;
import com.mybatis.test.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;


    @RequestMapping("/findAll")
    @ResponseBody
    public String findAll(){
        ArrayList<Student> students = studentService.findAll();
        return students.toString();
    }

    @RequestMapping("/insert")
    @ResponseBody
    public Student insert(){
       /* boolean isInsert = studentService.save(
                new Student(1,"Test","M","1990","ACC"));
        return isInsert;*/
        return studentService.save(
                new Student(1,"Test","M","1990","ACC"));
    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public boolean delete(@PathVariable int id){
        boolean isDelete = studentService.delete(id);
        return isDelete;
    }

}
