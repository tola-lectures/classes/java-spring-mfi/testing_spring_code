package org.spring.data.jpa.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "product_name", length = 100, nullable = false)
    private String name;
    @Column(name = "product_code", length = 100, nullable = false, unique = true)
    private String code;
    private String status;

    @OneToOne
    private ProductImage productImage;

    @ManyToOne
    private Category category;

    @ManyToMany
    @JoinTable(
            name ="product_orders",
            joinColumns = {@JoinColumn(name="product_id")},
            inverseJoinColumns = {@JoinColumn(name = "order_id")}
    )
    private List<OrderItem> orderItemList;









}
