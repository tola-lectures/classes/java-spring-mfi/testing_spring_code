package com.spring.data.jpa.services;

import com.spring.data.jpa.models.Post;

import java.util.List;

public interface PostService {

    List<Post> findAll();
    Post findById(Long id);
    Post insert(Post post);
    Post update(Post post);

}
