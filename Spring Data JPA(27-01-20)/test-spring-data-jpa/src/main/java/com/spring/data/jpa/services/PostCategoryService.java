package com.spring.data.jpa.services;

import com.spring.data.jpa.models.Post;
import com.spring.data.jpa.models.PostCategory;

import java.util.List;

public interface PostCategoryService {

    List<PostCategory> findAll();
    PostCategory findById(Long id);
    PostCategory insert(PostCategory postCategory);
    PostCategory update(PostCategory postCategory);


}
