package com.spring.data.jpa.services.impl;

import com.spring.data.jpa.models.User;
import com.spring.data.jpa.repositories.UserRepository;
import com.spring.data.jpa.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User insert(User user) {
        return userRepository.save(user);
    }

    @Override
    public User update(User user) {
        //TODO: Find user by Id that we wan to  update
        User getUser = userRepository.getOne(user.getId());
        if(getUser != null){
            getUser.setUsername(user.getUsername());
            getUser.setEmail(user.getEmail());
            getUser.setDob(user.getDob());
            User saveUser = userRepository.save(getUser);
            if(saveUser != null){
                return saveUser;
            }
        }
        return null;
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).get();
        //return userRepository.getOne(id);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public List<User> customQueryFindByUsername(String username) {
        return userRepository.customQueryFindByUsername(username);
    }

    @Override
    public List<User> customQueryFindByUsernameNative(String username) {
        return userRepository.customQueryFindByUsernameNative(username);
    }
}
