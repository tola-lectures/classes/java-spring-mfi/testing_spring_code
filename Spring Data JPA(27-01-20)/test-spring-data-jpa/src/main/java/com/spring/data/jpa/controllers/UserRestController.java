/*
package com.spring.data.jpa.controllers;

import com.spring.data.jpa.models.User;
import com.spring.data.jpa.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

//@Controller
@RestController
public class UserRestController {

    @Autowired
    private UserService userService;

    private Map<String, Object> map;

    //@ResponseBody
    @RequestMapping(value = "/find-all", method = RequestMethod.GET)
    public Map<String, Object> findAll(){
        map = new HashMap<>();
        map.put("user", userService.findAll());
        return map;
    }

    @RequestMapping(value = "/insert")
    public Map<String, Object> insert(){
        map = new HashMap<>();
        User userFrm = new User();
        userFrm.setId(1L);
        userFrm.setUsername("Dara");
        userFrm.setEmail("data@gamil.com");
        userFrm.setGender("F");
        userFrm.setPassword("******");
        userFrm.setStatus("0");
        userFrm.setDob("1990");
        User user = userService.save(userFrm);
        if(user != null){
            map.put("user", user);
            map.put("message", "User has been inserted successfully!");
        }else{
            map.put("message", "ERROR!!!! User has not been inserted!!!!!");
        }
        return map;
    }

    @RequestMapping(value = "/update")
    public Map<String, Object> update(){
        map = new HashMap<>();
        User userFrm = new User();
        userFrm.setId(1L);
        userFrm.setEmail("data1993@gamil.com");

        User findUserById = userService.findById(userFrm.getId());

        if(findUserById != null){
            findUserById.setEmail(userFrm.getEmail());
            User user = userService.save(findUserById);
            if(user != null){
                map.put("user", user);
                map.put("message", "User has been updated successfully!");
            }else{
                map.put("message", "ERROR!!!! User has not been updated!!!!!");
            }
        }else{
            map.put("message", "User not found!!!!");
        }
        return map;
    }

}
*/
