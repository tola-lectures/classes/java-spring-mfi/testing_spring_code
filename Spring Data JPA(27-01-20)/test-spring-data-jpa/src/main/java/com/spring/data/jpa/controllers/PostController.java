package com.spring.data.jpa.controllers;

import com.spring.data.jpa.models.Post;
import com.spring.data.jpa.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostService postService;
    private Map<String, Object> map;

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Map<String, Object> insert(@RequestBody Post post){
        map = new HashMap<>();
        map.put("post" , postService.insert(post));
        return map;
    }

    @RequestMapping(value = "/find-all", method = RequestMethod.GET)
    public Map<String, Object> findAll(){
        map = new HashMap<>();
        map.put("post" , postService.findAll());
        return map;
    }

}
