package com.spring.data.jpa.controllers;

import com.spring.data.jpa.models.Post;
import com.spring.data.jpa.models.PostCategory;
import com.spring.data.jpa.services.PostCategoryService;
import com.spring.data.jpa.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/post-category")
public class PostCategoryController {

    @Autowired
    private PostCategoryService postCategoryService;
    private Map<String, Object> map;

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Map<String, Object> insert(@RequestBody PostCategory postCategory){
        map = new HashMap<>();
        map.put("post" , postCategoryService.insert(postCategory));
        return map;
    }

    @RequestMapping(value = "/find-all", method = RequestMethod.GET)
    public Map<String, Object> findAll(){
        map = new HashMap<>();
        map.put("post" , postCategoryService.findAll());
        return map;
    }

}
