package com.spring.data.jpa.services;

import com.spring.data.jpa.models.User;

import java.util.List;

public interface UserService {

    User insert(User user);
    User update(User user);
    User findById(Long id);
    List<User> findAll();
    void deleteById(Long id);
    List<User> customQueryFindByUsername(String username);
    List<User> customQueryFindByUsernameNative(String username);


}
