package com.spring.data.jpa.controllers;

import com.spring.data.jpa.models.User;
import com.spring.data.jpa.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

//@Controller
@RestController
public class UserRestControllerV2 {

    @Autowired
    private UserService userService;

    private Map<String, Object> map;

    //@ResponseBody
    @RequestMapping(value = "/find-all", method = RequestMethod.GET)
    public Map<String, Object> findAll(){
        map = new HashMap<>();
        map.put("user", userService.findAll());
        return map;
    }

    @RequestMapping("cq-find-by-username")
    public Map<String, Object> customQueryFindUserByUsername(
            @RequestParam(name = "username", required = false) String username){
        map = new HashMap<>();
        map.put("", userService.customQueryFindByUsernameNative(username));
        return map;
    }

    @RequestMapping(value = "/find-all-with-page", method = RequestMethod.GET)
    public Map<String, Object> findAllWithPagination(
            @RequestParam(name = "page", required = false, defaultValue = "0") Long page
    ){
        System.out.println(page);
        map = new HashMap<>();
        map.put("user", userService.findAll());
        return map;
    }

    @RequestMapping(value = "/find-by-id/{id}", method = RequestMethod.GET)
    public Map<String, Object> findById(@PathVariable("id") Long id){
        map = new HashMap<>();
        map.put("user", userService.findById(id));
        return map;
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Map<String, Object> insert(@RequestBody @Valid User user, BindingResult bindingResult){
        map = new HashMap<>();
        if(bindingResult.hasErrors()){
            map.put("error", bindingResult.getFieldError());
            return map;
        }
        User insertUser = userService.insert(user);
        if(insertUser != null){
            map.put("user", user);
            map.put("message", "User has been inserted successfully!");
        }else{
            map.put("message", "ERROR!!!! User has not been inserted!!!!!");
        }
        return map;
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Map<String, Object> update(@RequestBody User user){
        map = new HashMap<>();
        User updateUser = userService.update(user);
        if(updateUser != null){
            map.put("user", user);
            map.put("message", "User has been updated successfully!");
        }else{
            map.put("message", "ERROR!!!! User has not been updated!!!!!");
        }
        return map;
    }

}
