package com.spring.data.jpa.repositories;

import com.spring.data.jpa.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends /*CrudRepository<User, Long>*/
        /*PagingAndSortingRepository<User, Long>*/
        JpaRepository<User, Long> {

                //TODO: Custom QUERY with JPQL
        @Query("From User WHERE username=?1")
        List<User> customQueryFindByUsername(String username);

        @Query(value="Select * from  users WHERE username=?1", nativeQuery=true)
        List<User> customQueryFindByUsernameNative(String username);

}
