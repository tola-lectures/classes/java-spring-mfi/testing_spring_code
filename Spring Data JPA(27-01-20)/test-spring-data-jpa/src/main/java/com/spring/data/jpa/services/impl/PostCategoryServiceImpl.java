package com.spring.data.jpa.services.impl;

import com.spring.data.jpa.models.Post;
import com.spring.data.jpa.models.PostCategory;
import com.spring.data.jpa.repositories.PostCategoryRepository;
import com.spring.data.jpa.services.PostCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostCategoryServiceImpl implements PostCategoryService {

    @Autowired
    private PostCategoryRepository postCategoryRepository;

    @Override
    public List<PostCategory> findAll() {
        return postCategoryRepository.findAll();
    }

    @Override
    public PostCategory findById(Long id) {
        return postCategoryRepository.findById(id).get();
    }

    @Override
    public PostCategory insert(PostCategory postCategory) {
        return postCategoryRepository.save(postCategory);
    }

    @Override
    public PostCategory update(PostCategory postCategory) {
        return postCategoryRepository.save(postCategory);
    }
}
