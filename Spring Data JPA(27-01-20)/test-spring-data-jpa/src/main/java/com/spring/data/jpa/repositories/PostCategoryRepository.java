package com.spring.data.jpa.repositories;

import com.spring.data.jpa.models.PostCategory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;

@Repository
public interface PostCategoryRepository extends JpaRepository<PostCategory, Long> {


}
