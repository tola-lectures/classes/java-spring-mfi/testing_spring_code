package com.spring.data.jpa.repositories;

import com.spring.data.jpa.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {




}
