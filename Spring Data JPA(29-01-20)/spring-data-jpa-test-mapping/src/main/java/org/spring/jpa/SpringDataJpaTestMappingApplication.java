package org.spring.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataJpaTestMappingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataJpaTestMappingApplication.class, args);
    }

}
