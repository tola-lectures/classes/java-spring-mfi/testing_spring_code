package org.spring.jpa.models.company;

import lombok.Getter;
import lombok.Setter;
import org.spring.jpa.models.base.BaseEntity;
import org.spring.jpa.models.customer.Customer;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;

@Setter
@Getter
@Entity
public class Department extends BaseEntity {

    private String name;

    /*@OneToMany
    @JoinColumn(name = "employee_id")
    private List<Employee> employees;

    @OneToMany
    @JoinColumn(name = "customer_id")
    private List<Customer> customer;*/


}
