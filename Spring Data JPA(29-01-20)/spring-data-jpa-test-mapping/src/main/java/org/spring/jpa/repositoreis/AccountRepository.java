package org.spring.jpa.repositoreis;

import org.spring.jpa.models.bank.Account;
import org.spring.jpa.models.dto.AccountDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long > {

    @Query("SELECT new org.spring.jpa.models.dto.AccountDTO(a.accountNumber, a.balance) " +
            "FROM Account a")
    List<AccountDTO> findJPQL();



}
