package org.spring.jpa.models.bank;

import lombok.Getter;
import lombok.Setter;
import org.spring.jpa.models.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class Deposit extends BaseEntity {

    private long amount;
    private String type;
    @ManyToOne
    private Account account;

}
