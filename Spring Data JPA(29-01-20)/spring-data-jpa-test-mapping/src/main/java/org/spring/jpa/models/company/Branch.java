package org.spring.jpa.models.company;

import lombok.Getter;
import lombok.Setter;
import org.spring.jpa.models.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * @author tola on 1/29/20
 **/
@Getter
@Setter
@Entity
public class Branch extends BaseEntity {

    private String name;
    private String country;
    private String city;
    private String phone;
    private String address;

    /*@OneToMany
    @JoinColumn(name = "employee_id")
    private List<Employee> employees;*/


}
