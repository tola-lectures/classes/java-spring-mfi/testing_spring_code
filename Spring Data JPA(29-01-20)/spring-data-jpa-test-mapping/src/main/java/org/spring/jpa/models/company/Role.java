package org.spring.jpa.models.company;

import lombok.Getter;
import lombok.Setter;
import org.spring.jpa.models.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

@Getter
@Setter
@Entity
public class Role extends BaseEntity {

    private String name;

    @ManyToMany
    @JoinTable(name = "employee_roles",
        joinColumns = {@JoinColumn(name = "employee_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    private List<Employee> employees;




}
