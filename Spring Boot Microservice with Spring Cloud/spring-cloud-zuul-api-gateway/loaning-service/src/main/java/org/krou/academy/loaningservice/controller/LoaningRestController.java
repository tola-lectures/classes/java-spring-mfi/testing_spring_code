package org.krou.academy.loaningservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/loan")
public class LoaningRestController {

    @GetMapping
    public String findAll(){
        return "LoaningRestController";
    }

}
