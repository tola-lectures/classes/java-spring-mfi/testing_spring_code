package org.krou.academy.loaningservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class LoaningServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoaningServiceApplication.class, args);
    }

}
