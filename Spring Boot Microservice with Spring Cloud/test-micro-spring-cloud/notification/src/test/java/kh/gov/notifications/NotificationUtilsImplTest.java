package kh.gov.notifications;

import org.krou.academy.notifications.NotificationUtils;
import org.krou.academy.notifications.NotificationUtilsImpl;
import org.krou.academy.notifications.fcm.servies.PushNotificationService;
import org.krou.academy.notifications.mail.models.Mail;
import org.krou.academy.notifications.mail.services.MailService;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author tola on 1/6/20
 **/

@SpringBootConfiguration
@EnableAutoConfiguration
@SpringBootTest(
        classes = NotificationUtilsImpl.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NotificationUtilsImplTest {

    @MockBean
    private PushNotificationService pushNotificationService;

    @MockBean
    private MailService mailService;

    @Autowired
    private NotificationUtils notificationUtils;

    @Rule
    public SmtpServerRule smtpServerRule = null;

    @Test
    public void sendMail() throws MessagingException, IOException {
        System.out.println("sendMail");
        Mail mail = new Mail();
        mail.setMailFrom("tola.test1993@gmail.com");
        mail.setMailTo("tolapheng99@gmail.com");
        mail.setMailSubject("Spring Boot - Email Example");
        mail.setMailContent("Learn How to send Email using Spring Boot!");
        notificationUtils.sendEmail(mail);
        smtpServerRule = new SmtpServerRule(2525);
        MimeMessage[] receivedMessages = smtpServerRule.getMessages();
        assertEquals(1, receivedMessages.length);
        MimeMessage current = receivedMessages[0];
        assertEquals(mail.getMailSubject(), current.getSubject());
        assertEquals(mail.getMailTo(), current.getAllRecipients()[0].toString());
        assertTrue(String.valueOf(current.getContent()).contains(mail.getMailContent()));
    }

  /*  @Test
    public void sendNotification(){
        Notification  notification = new Notification();
        notification.setBody("body");
        notification.setTitle("title");
        Push push = new Push("test", "test", notification);
        notificationUtils.pushNotification(push);
    }
*/

}
