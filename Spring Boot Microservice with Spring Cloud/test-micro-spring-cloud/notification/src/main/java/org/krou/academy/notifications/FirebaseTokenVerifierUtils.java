package org.krou.academy.notifications;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author tola on 1/17/20
 **/
@Service
public class FirebaseTokenVerifierUtils {

    private static final Logger logger = LoggerFactory.getLogger(FirebaseTokenVerifierUtils.class);

    @Value("${firebase.jwt.publickeyurl}")
    private String pubKeyUrl;

    @Value("${firebase.jwt.issuer}")
    private String jwtIssuer;

    /**
     * @param token
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public String verify(String token) throws IOException {
        // get public keys
        JsonObject publicKeys = getPublicKeysJson();

        // verify count
        int size = publicKeys.entrySet().size();
        int count = 0;

        // get json object as map
        // loop map of keys finding one that verifies
        for (Map.Entry<String, JsonElement> entry : publicKeys.entrySet()) {
            // log
            logger.info("attempting jwt id token validation with: ");

            try {
                // trying next key
                count++;

                //System.out.println("entry ===> " + entry);

                // get public key
                PublicKey publicKey = getPublicKey(entry);

                // validate claim set
                Claims claims = (Claims) Jwts.parser().setSigningKey(publicKey).parse(token).getBody();
                Date expiration = claims.getExpiration();
                //String issuer = claims.getIssuer();
                //System.out.println("issuer ===> " + issuer.lastIndexOf("/"));
                //System.out.println("issuer ===> " + issuer.substring(issuer.lastIndexOf("/"), issuer.length()) );

                //System.out.println("expiration ===> " + expiration);
                //System.out.println("now ===> " + new Date());
                if (expiration.after(new Date()) && claims.getIssuer().equals(jwtIssuer)) {
                    // success, we can return
                    return claims.get("phone_number").toString();
                } else {
                    System.out.println("Firebase ID token has expired or is not yet valid. Get a fresh ID token and try again.");
                }
            } catch (Exception e) {
                // log
                logger.info("Firebase id token verification error");
                logger.info(e.getMessage());
                // claims may have been tampered with
                // if this is the last key, return false
                if (count == size) {
                    return null;
                }
            }
        }

        // no jwt exceptions
        return null;
    }

    public Map<String, Object> getPayload(String token) throws IOException {
        Map<String, Object> map = new HashMap<>();
        JsonObject publicKeys = getPublicKeysJson();
        int size = publicKeys.entrySet().size();
        int count = 0;
        for (Map.Entry<String, JsonElement> entry : publicKeys.entrySet()) {
            try {
                count++;
                PublicKey publicKey = getPublicKey(entry);
                Claims claims = (Claims) Jwts.parser().setSigningKey(publicKey).parse(token).getBody();
                Date expiration = claims.getExpiration();
                if (expiration.after(new Date()) && claims.getIssuer().equals(jwtIssuer)) {
                    map.put("payload", claims);
                    map.put("message", "Firebase id token verified successfully.");
                } else {
                    map.put("payload", null);
                    map.put("message", "Firebase ID token has expired or is not yet valid. Get a fresh ID token and try again.");
                }
            } catch (Exception e) {
                logger.info("Firebase id token verification error");
                logger.info(e.getMessage());
                if (count == size) {
                    map.put("payload", null);
                    map.put("message", "Firebase ID token has expired or is not yet valid. Get a fresh ID token and try again.");
                    return map;
                }
            }
        }
        return map;
    }

    /**
     * @param entry
     * @return
     * @throws GeneralSecurityException
     */
    private PublicKey getPublicKey(Map.Entry<String, JsonElement> entry) throws GeneralSecurityException, IOException {
        String publicKeyPem = entry.getValue().getAsString()
                .replaceAll("-----BEGIN (.*)-----", "")
                .replaceAll("-----END (.*)----", "")
                .replaceAll("\r\n", "")
                .replaceAll("\n", "")
                .trim();

        logger.info(publicKeyPem);

        // generate x509 cert
        InputStream inputStream = new ByteArrayInputStream(entry.getValue().getAsString().getBytes("UTF-8"));
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate) cf.generateCertificate(inputStream);
        return cert.getPublicKey();
    }


    /**
     * @return
     * @throws IOException
     */
    private JsonObject getPublicKeysJson() throws IOException {
        // get public keys
        URI uri = URI.create(pubKeyUrl);
        GenericUrl url = new GenericUrl(uri);
        HttpTransport http = new NetHttpTransport();
        HttpResponse response = http.createRequestFactory().buildGetRequest(url).execute();

        // store json from request
        String json = response.parseAsString();
        // disconnect
        response.disconnect();

        // parse json to object
        JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
        System.out.println(jsonObject);
        return jsonObject;
    }


}
