package org.krou.academy.notifications;

import com.google.firebase.auth.FirebaseAuthException;
import org.krou.academy.notifications.fcm.models.FirebaseResponse;
import org.krou.academy.notifications.fcm.models.frms.PushNotificationFrm;
import org.krou.academy.notifications.mail.models.Mail;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

/**
 * @author tola on 1/6/20
 **/
public interface NotificationUtils {

    FirebaseResponse pushNotificationByDeviceIdAndTokenId(PushNotificationFrm pushNotificationFrm) throws FirebaseAuthException, IOException, GeneralSecurityException;

    Map<String, Object> sendMessageByToken(PushNotificationFrm frm);

    void sendEmail(Mail mail);


}
