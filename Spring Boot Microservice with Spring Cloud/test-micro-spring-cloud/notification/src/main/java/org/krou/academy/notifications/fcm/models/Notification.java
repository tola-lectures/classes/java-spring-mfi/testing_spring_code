package org.krou.academy.notifications.fcm.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tola on 12/25/19
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notification {

    private String title;
    private String body;


}
