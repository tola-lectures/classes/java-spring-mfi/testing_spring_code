package org.krou.academy.notifications.fcm.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author tola on 12/25/19
 **/
@Data
public class Push {

    private String to;

    private String priority;

    private Notification notification;

    @JsonProperty(value = "registration_ids")
    private List<String> registrationIds;

    /**
     * @param priority
     * @param notification
     * @param registrationIds
     */
    public Push(String priority, Notification notification, List<String> registrationIds) {
        this.priority = priority;
        this.notification = notification;
        this.registrationIds = registrationIds;
    }

    /**
     * @param to
     * @param priority
     * @param notification
     */
    public Push(String to, String priority, Notification notification) {
        this.to = to;
        this.priority = priority;
        this.notification = notification;
    }


}
