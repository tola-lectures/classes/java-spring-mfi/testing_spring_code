package org.krou.academy.notifications.mail.models;

/**
 * @author tola on 1/6/20
 **/

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Data
@AllArgsConstructor
@Entity
@Table(name = "mail")
public class Mail {

    @Id
    private int id;

    private String mailFrom;

    private String mailTo;

    private String mailCc;

    private String mailBcc;

    private String mailSubject;

    private String mailContent;

    private String contentType;

    //private List <Object> attachments;

    public Mail() {
        contentType = "text/plain";
    }


}
