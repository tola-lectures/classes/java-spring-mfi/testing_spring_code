package org.krou.academy.notifications.fcm.models.frms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tola on 1/13/20
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PushNotificationFrm {

    private String deviceId;
    private String token;
    //private String personalCode;
    private String title;
    private String message;
}
