package org.krou.academy.notifications.mail.services;

import org.krou.academy.notifications.mail.models.Mail;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author tola on 1/6/20
 **/
@SpringBootApplication
@ComponentScan("org.krou.academy.notifications.mail")
public class Application {


    public static void main(String[] args) {

        Mail mail = new Mail();
        mail.setMailFrom("sssss@gmail.com");
        mail.setMailTo("to@gmail.com");
        mail.setMailSubject("Spring Boot - Email Example");
        mail.setMailContent("Learn How to send Email using Spring Boot!");

        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        MailService mailService = (MailService) ctx.getBean("mailService");
        mailService.sendEmail(mail);

    }
}
