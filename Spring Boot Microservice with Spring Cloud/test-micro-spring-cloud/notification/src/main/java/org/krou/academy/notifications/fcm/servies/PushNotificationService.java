package org.krou.academy.notifications.fcm.servies;

import org.krou.academy.notifications.fcm.models.FirebaseResponse;
import org.krou.academy.notifications.fcm.models.frms.PushNotificationFrm;

import java.util.Map;

/**
 * @author tola on 1/2/20
 **/
public interface PushNotificationService {

    FirebaseResponse pushNotificationByDeviceIdAndTokenId(PushNotificationFrm frm);

    Map<String, Object> sendMessageByToken(PushNotificationFrm frm);

}
