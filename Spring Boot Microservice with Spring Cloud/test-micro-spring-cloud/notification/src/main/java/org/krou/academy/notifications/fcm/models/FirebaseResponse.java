package org.krou.academy.notifications.fcm.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author tola on 12/25/19
 **/
@Data
public class FirebaseResponse {

    private static final long serialVersionUID = 1L;
    private boolean status;
    private String message;


}
