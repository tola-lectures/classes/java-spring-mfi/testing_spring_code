package org.krou.academy.notifications.fcm.servies.impl;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import org.krou.academy.notifications.FirebaseTokenVerifierUtils;
import org.krou.academy.notifications.fcm.configurations.HeaderRequestInterceptor;
import kh.gov.camdx.notifications.fcm.models.*;
import org.krou.academy.notifications.fcm.models.frms.PushNotificationFrm;
import org.krou.academy.notifications.fcm.servies.PushNotificationService;
import org.krou.academy.notifications.fcm.models.FirebaseResponse;
import org.krou.academy.notifications.fcm.models.Notification;
import org.krou.academy.notifications.fcm.models.Push;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author tola on 12/25/19
 **/

@Service("pushNotificationService")
public class PushNotificationServiceImpl implements PushNotificationService {

    @Value("${FIREBASE_SERVER_KEY}")
    private String fcmKey;

    @Value("${FCM_API}")
    private String FCM_API;

    @Autowired
    FirebaseTokenVerifierUtils firebaseAuthVerifier;

    @Override
    public FirebaseResponse pushNotificationByDeviceIdAndTokenId(PushNotificationFrm frm) {
        FirebaseResponse firebaseResponse = null;
        try {
            List<String> tokens = new ArrayList<>();
            tokens.add(frm.getToken());
            Notification notification = new Notification(frm.getTitle(), frm.getMessage());
            Push push = new Push("high", notification, tokens);
            firebaseResponse = this.sendNotification(push);
            if (firebaseResponse.isStatus()) {
                firebaseResponse.setMessage("Notification has been sent. " + firebaseResponse.getMessage());
                firebaseResponse.setStatus(true);
            } else {
                firebaseResponse.setMessage("ERROR! Notification has not been sent." + firebaseResponse.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            firebaseResponse.setMessage("ERROR! Notification has not been sent." + firebaseResponse.getMessage() + " | " + e.getMessage());
        }
        return firebaseResponse;
    }


    private FirebaseResponse sendNotification(Push push) {
        HttpEntity<Push> request = new HttpEntity<>(push);

        System.out.println(push.toString());
        System.out.println(fcmKey);
        System.out.println(FCM_API);

        CompletableFuture<FirebaseResponse> pushNotification = this.send(request);
        CompletableFuture.allOf(pushNotification).join();

        FirebaseResponse firebaseResponse = null;

        try {
            firebaseResponse = pushNotification.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return firebaseResponse;
    }


    /**
     * send push notification to API FCM
     * <p>
     * Using CompletableFuture with @Async to provide Asynchronous call.
     *
     * @param entity
     * @return
     */
    @Async
    CompletableFuture<FirebaseResponse> send(HttpEntity<Push> entity) {

        RestTemplate restTemplate = new RestTemplate();

        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

        interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + fcmKey));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
        restTemplate.setInterceptors(interceptors);
        FirebaseResponse firebaseResponse = restTemplate.postForObject(FCM_API, entity, FirebaseResponse.class);
        return CompletableFuture.completedFuture(firebaseResponse);
    }

    @Override
    public Map<String, Object> sendMessageByToken(PushNotificationFrm frm) {
        Map<String, Object> map = new HashMap<>();
        String response = "";
        try {
            // See documentation on defining a message payload.
            Message message = Message.builder()
                    .putData("device_id", frm.getDeviceId())
                    .putData("token", frm.getToken())
                    .putData("title", frm.getTitle())
                    .putData("message", frm.getMessage())
                    .setToken(frm.getToken())
                    .build();
            // Send a message to the devices subscribed to the provided topic.
            response = FirebaseMessaging.getInstance().send(message);
            map.put("response_message", response);
            map.put("message", "It has been pushed to device token << " + frm.getToken() + " >>");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("response_message", response);
            map.put("message", "Error << " + frm.getToken() + " >>");
        }
        return map;
    }


}
