package org.krou.academy.notifications.verifiedcode;

import org.apache.commons.codec.digest.DigestUtils;


/**
 * @author tola on 1/15/20
 **/
public class DigestCalculator {

    public static byte[] calculateDigest(byte[] dataToDigest, HashType hashType) {
        String algorithmName = hashType.getAlgorithmName();
        return DigestUtils.getDigest(algorithmName).digest(dataToDigest);
    }
}
