package org.krou.academy.notifications.mail.services;

import org.krou.academy.notifications.mail.models.Mail;

/**
 * @author tola on 1/6/20
 **/
public interface MailService {

    void sendEmail(Mail mail);

}
