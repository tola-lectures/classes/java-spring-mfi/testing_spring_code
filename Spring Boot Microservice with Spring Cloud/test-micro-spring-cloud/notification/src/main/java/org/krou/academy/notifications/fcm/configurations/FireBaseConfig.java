package org.krou.academy.notifications.fcm.configurations;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author tola on 1/17/20
 **/
@Configuration
public class FireBaseConfig {

    @Value("${FCM_ADMINSDK}")
    private String FCM_ADMINSDK;

    @Bean
    public FirebaseApp createFireBaseApp() throws IOException {

        File file = ResourceUtils.getFile(FCM_ADMINSDK);

        FileInputStream serviceAccount =
                new FileInputStream(file);

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://your-app.firebaseio.com")
                .build();

        return FirebaseApp.initializeApp(options);
    }

    @Bean
    @Qualifier("main")
    public DatabaseReference provideDatabaseReference(FirebaseApp firebaseApp) {
        FirebaseDatabase.getInstance(firebaseApp)
                .setPersistenceEnabled(false);
        return FirebaseDatabase
                .getInstance(firebaseApp)
                .getReference();
    }


}
