package org.krou.academy.notifications;

import com.google.firebase.auth.FirebaseAuthException;
import org.krou.academy.notifications.fcm.models.FirebaseResponse;
import org.krou.academy.notifications.fcm.models.frms.PushNotificationFrm;
import org.krou.academy.notifications.fcm.servies.PushNotificationService;
import org.krou.academy.notifications.mail.models.Mail;
import org.krou.academy.notifications.mail.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Map;

/**
 * @author tola on 1/6/20
 **/
@Component
public class NotificationUtilsImpl implements NotificationUtils {

    @Autowired
    private PushNotificationService pushNotificationService;

    @Autowired
    private MailService mailService;

    @Override
    public FirebaseResponse pushNotificationByDeviceIdAndTokenId(PushNotificationFrm pushNotificationFrm) throws FirebaseAuthException, IOException, GeneralSecurityException {
        return pushNotificationService.pushNotificationByDeviceIdAndTokenId(pushNotificationFrm);
    }

    @Override
    public Map<String, Object> sendMessageByToken(PushNotificationFrm frm) {
        return pushNotificationService.sendMessageByToken(frm);
    }

    @Override
    public void sendEmail(Mail mail) {
        mailService.sendEmail(mail);
    }
}
