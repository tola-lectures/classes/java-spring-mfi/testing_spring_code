package org.krou.academy.loaningservice.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/loan")
public class LoanRestController {

    @GetMapping
    public String findAll(){
        return "find all loan";
    }

}
