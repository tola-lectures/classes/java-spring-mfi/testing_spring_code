package org.spring.restful.services.impl;

import org.spring.restful.models.user.User;
import org.spring.restful.models.views.UserView;
import org.spring.restful.repositories.UserRepository;
import org.spring.restful.services.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userService.findByUsername(username);

        //UserView userView = userService.findUserViewByUsername(username);
        //User user = null;
        //if(userView != null){
        //    user = new User();
        //    BeanUtils.copyProperties(userView , user);
        //}
        if(user == null){
            System.out.println("User not found!!!!!!");
            throw  new UsernameNotFoundException("User not found!!!!!");
        }else {
            System.out.println("User has found!!!!!!");
        }
        return  user;
    }
}
