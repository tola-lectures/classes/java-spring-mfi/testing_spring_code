package org.spring.restful.models.views;

import org.springframework.security.core.GrantedAuthority;

public interface RoleView  {

    long getId();
    String getName();


}
