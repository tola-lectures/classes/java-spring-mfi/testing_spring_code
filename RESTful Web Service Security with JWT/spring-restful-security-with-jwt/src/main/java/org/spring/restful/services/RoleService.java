package org.spring.restful.services;

import org.spring.restful.models.user.Role;

public interface RoleService {

    Role save(Role role);

}
