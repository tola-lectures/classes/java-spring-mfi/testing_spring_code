package org.spring.restful.models.bank;

import lombok.Getter;
import lombok.Setter;
import org.spring.restful.models.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class Withdraw extends BaseEntity {

    private long amount;
    private String type;
    @ManyToOne
    private Account account;

}
