package org.spring.restful;

import org.spring.restful.models.user.Role;
import org.spring.restful.models.user.User;
import org.spring.restful.repositories.RoleRepository;
import org.spring.restful.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SpringRestfulDemoApplication implements CommandLineRunner {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public static void main(String[] args) {
        SpringApplication.run(SpringRestfulDemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        // TODO : Insert two roles into database
        Role role1 = new Role();
        role1.setName("ROLE_ADMIN");
        Role role2 = new Role();
        role2.setName("ROLE_CLIENT");
        roleRepository.save(role1);
        roleRepository.save(role2);

        User user = new User();
        user.setUsername("admin");
        user.setEmail("admin@gmail.com");
        user.setPassword(passwordEncoder.encode("123456"));

        Role roleUser1 = new Role();
        roleUser1.setId(1);
        Role roleUser2 = new Role();
        roleUser2.setId(2);
        List<Role> roles = new ArrayList<>();
        roles.add(roleUser1);
        roles.add(roleUser2);

        user.setRoles(roles);

        User saved = userService.insert(user);
        if(saved != null){
            System.out.println("****** Saved *********");
        }else {
            System.out.println("****** Error *********");
        }


        User userClient = new User();
        userClient.setUsername("client");
        userClient.setEmail("client@gmail.com");
        userClient.setPassword(passwordEncoder.encode("123456"));


        Role roleUserClient = new Role();
        roleUserClient.setId(2);
        roles = new ArrayList<>();
        roles.add(roleUserClient);

        userClient.setRoles(roles);

        User savedClient = userService.insert(userClient);
        if(savedClient != null){
            System.out.println("****** Saved userClient *********");
        }else {
            System.out.println("****** Error userClient *********");
        }

       // User findByEmail = userService.findUserByUsername("admin");
        //System.out.println(findByEmail.toString());



    }

}
