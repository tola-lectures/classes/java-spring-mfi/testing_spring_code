package org.spring.restful.repositories;

import org.spring.restful.models.user.User;
import org.spring.restful.models.views.UserView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findUserByUsername(String username);
    UserView findUserViewByUsername(String username);
    UserView findUserViewById(long id);
    Page<UserView> findBy(Pageable pageable);

    User findByUsername(String username);



}
