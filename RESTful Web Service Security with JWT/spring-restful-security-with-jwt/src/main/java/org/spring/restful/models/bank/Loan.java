package org.spring.restful.models.bank;

import lombok.Getter;
import lombok.Setter;
import org.spring.restful.models.base.BaseEntity;
import org.spring.restful.models.customer.Customer;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
@Entity
public class Loan extends BaseEntity {

    private long amount;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    private long interestRate;
    private String type;
    private long duration;
    @ManyToOne
    private Customer customer;

}
