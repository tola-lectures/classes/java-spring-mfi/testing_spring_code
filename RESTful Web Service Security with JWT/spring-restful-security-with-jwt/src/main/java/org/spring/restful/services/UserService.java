package org.spring.restful.services;

import org.spring.restful.models.user.User;
import org.spring.restful.models.views.UserView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface UserService {

    User insert(User user);
    UserView update(User user);
    long delete(long id);

    UserView findUserViewByUsername(String username);
    UserView findUserViewById(long id);
    Page<UserView> findBy(Pageable pageable);

    User findByUsername(String username);












}
