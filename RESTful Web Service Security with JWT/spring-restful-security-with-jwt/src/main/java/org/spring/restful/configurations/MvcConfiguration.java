package org.spring.restful.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfiguration implements WebMvcConfigurer {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        // TODO: Access Denied page
        registry.addViewController("/errors/403").setViewName("/errors/403");
        registry.addViewController("/errors/401").setViewName("/errors/401");

        // TODO: Authentication page
        registry.addViewController("/login").setViewName("/authentications/login");

    }
}
