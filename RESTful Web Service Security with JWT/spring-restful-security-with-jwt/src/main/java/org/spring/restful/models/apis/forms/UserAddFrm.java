package org.spring.restful.models.apis.forms;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Setter
@Getter
public class UserAddFrm {

    @NotEmpty(message = "Username is required!")
    private String username;
    @NotEmpty(message = "Email is required!")
    private String email;
    @NotEmpty(message = "Password is required!")
    private String password;
    @NotNull(message = "Status is required!")
    private boolean status;
    @NotNull(message = "roles is required!")
    private List<Long> roleId;

}
