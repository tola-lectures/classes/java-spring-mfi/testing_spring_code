package org.spring.restful.models.views;


import java.util.List;

public interface UserView {

        long getId();
        String getUsername();
        String getPassword();
        String getEmail();
        List<RoleView> getRoles();

}
