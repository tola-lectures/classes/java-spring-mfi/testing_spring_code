package org.spring.restful.services.impl;

import org.spring.restful.models.user.Role;
import org.spring.restful.repositories.RoleRepository;
import org.spring.restful.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository repository;

    @Override
    public Role save(Role role) {
        return repository.save(role);
    }

}
