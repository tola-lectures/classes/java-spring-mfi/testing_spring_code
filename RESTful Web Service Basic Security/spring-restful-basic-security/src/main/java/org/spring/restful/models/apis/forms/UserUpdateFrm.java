package org.spring.restful.models.apis.forms;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Setter
@Getter
public class UserUpdateFrm {

    @NotNull(message = "User id is required.")
    private long id;
    private String username;
    private String email;
    private String password;
    private boolean status;
    private List<Long> roleId;

}
