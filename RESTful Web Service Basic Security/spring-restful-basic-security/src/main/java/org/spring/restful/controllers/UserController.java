package org.spring.restful.controllers;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.spring.restful.models.apis.ApiResponse;
import org.spring.restful.models.apis.Pagination;
import org.spring.restful.models.apis.forms.UserAddFrm;
import org.spring.restful.models.apis.forms.UserUpdateFrm;
import org.spring.restful.models.user.Role;
import org.spring.restful.models.user.User;
import org.spring.restful.models.views.UserView;
import org.spring.restful.services.UserServices;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserServices userServices;

    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "page" , dataType = "integer" , paramType = "query" , defaultValue = "0" , value = "Page number"),
                    @ApiImplicitParam(name = "size" , dataType = "integer" , paramType = "query" , defaultValue = "10" , value = "Record size")

            }
    )
    @GetMapping
    public ApiResponse<List<UserView>> findAll(@ApiIgnore Pageable pageable){
            ApiResponse<List<UserView>> userViewApiResponse = new ApiResponse<>();
            try{
                Page<UserView> userViews = userServices.findBy(pageable);

                List<UserView> users = userViews.getContent();
                if(users != null){
                    Pagination pagination = new Pagination();
                    pagination.setTotalPages(userViews.getTotalPages());
                    pagination.setTotalCount((int) userViews.getTotalElements());
                    pagination.setLimit(users.size());
                    pagination.setPage(userViews.getNumber());

                    userViewApiResponse.setData(users);
                    userViewApiResponse.setMessage("Data found.");
                    userViewApiResponse.setPagination(pagination);
                    userViewApiResponse.setCode(200);
                }else {
                    userViewApiResponse.setMessage("Data not found!");
                    userViewApiResponse.setCode(404);
                }
            }catch (Exception e){
                userViewApiResponse.setMessage("Error!");
                userViewApiResponse.setCode(500);
            }
            return userViewApiResponse;
    }

    @PostMapping
    public ApiResponse<User> insert(@Valid @RequestBody UserAddFrm userFrm){
        ApiResponse<User> apiResponse = new ApiResponse<>();
        try{
            User user = new User();
            BeanUtils.copyProperties(userFrm , user);
            List<Role> roles = new ArrayList<>();
            for (long roleId : userFrm.getRoleId()
                 ) {
                Role role = new Role();
                role.setId(roleId);
                roles.add(role);
            }
            user.setRoles(roles);
            User savedUser = userServices.insert(user);
            if(savedUser != null){
                apiResponse.setData(savedUser);
                apiResponse.setMessage("User has been saved.");
                apiResponse.setCode(201);
            }else {
                apiResponse.setMessage("User has not been saved.");
                apiResponse.setCode(500);
            }
        }catch (Exception e) {
            apiResponse.setMessage("Error!");
            apiResponse.setCode(500);
        }
        return apiResponse;
    }


    @GetMapping("/{id}")
    public ApiResponse<UserView> findById(@PathVariable("id") long id){
        ApiResponse<UserView> userViewApiResponse = new ApiResponse<>();
        try{
            UserView userViews = userServices.findUserViewById(id);
            if(userViews != null){
                userViewApiResponse.setData(userViews);
                userViewApiResponse.setMessage("Data found.");
                userViewApiResponse.setCode(200);
            }else {
                userViewApiResponse.setMessage("Data not found!");
                userViewApiResponse.setCode(404);
            }
        }catch (Exception e){
            userViewApiResponse.setMessage("Error!");
            userViewApiResponse.setCode(500);
        }
        return userViewApiResponse;
    }


    @PutMapping
    public ApiResponse<UserView> update(@Valid @RequestBody UserUpdateFrm userFrm){
        ApiResponse<UserView> apiResponse = new ApiResponse<>();
        try{
            User user = new User();
            BeanUtils.copyProperties(userFrm , user);
            List<Role> roles = new ArrayList<>();
            for (long roleId : userFrm.getRoleId()
            ) {
                Role role = new Role();
                role.setId(roleId);
                roles.add(role);
            }
            user.setRoles(roles);
            UserView updateUser = userServices.update(user);
            if(updateUser != null){
                apiResponse.setData(updateUser);
                apiResponse.setMessage("User has been updated.");
                apiResponse.setCode(201);
            }else {
                apiResponse.setMessage("User has not been updated.");
                apiResponse.setCode(500);
            }
        }catch (Exception e) {
            e.printStackTrace();
            apiResponse.setMessage("Error!");
            apiResponse.setCode(500);
        }
        return apiResponse;
    }


    //@RequestMapping(value = "", method =  RequestMethod.GET)
    /*@GetMapping("/{username}")
    public ResponseEntity<Map<String , Object>> findUserByUsername(@PathVariable("username") String username){
        Map<String , Object> map = new HashMap<>();
        try{
            UserView userView = userServices.findUserViewByUsername(username);
            if(userView != null){
                map.put("data" , userView);
                map.put("message" , "Success");
                map.put("status" , 200);
            }else {
                map.put("message" , "No record.");
                map.put("status" , 404);
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("message" , e.getMessage());
        }
        return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
    }

    @GetMapping("/map/{username}")
    public Map<String , Object> findUserByUsernameMap(@PathVariable("username") String username){
        Map<String , Object> map = new HashMap<>();
        try{
            UserView userView = userServices.findUserViewByUsername(username);
            if(userView != null){
                map.put("data" , userView);
                map.put("message" , "Success");
                map.put("status" , 200);
            }else {
                map.put("message" , "No record.");
                map.put("status" , 404);
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("message" , e.getMessage());
        }
        return map;
    }*/


}
