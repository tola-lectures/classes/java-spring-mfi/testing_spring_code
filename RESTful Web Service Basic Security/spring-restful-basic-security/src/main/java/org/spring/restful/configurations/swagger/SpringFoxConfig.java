package org.spring.restful.configurations.swagger;
/**
 *
 * @author tola on 9/10/19
 *
 **/
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Optional;


@Configuration
@EnableSwagger2
public class SpringFoxConfig {

    @Bean
    public Docket api() {

        //Adding Header
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder.name("Authorization")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue("Basic QVBJX1VTRVI6MTIzNDU2")        // based64 of - API_USER:123456
                .required(true)
                .build();
        java.util.List<Parameter> aParameters = new ArrayList<>();
        aParameters.add(aParameterBuilder.build());
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/v1/**"))
                .build()
                .apiInfo(metadata())
                .useDefaultResponseMessages(false)
                .globalOperationParameters(aParameters)
                .genericModelSubstitutes(Optional.class);
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Spring Boot RESTful Web Service")//
                .description("Spring Boot RESTful Web Service. You should click on the right top button `Authorize` and introduce it with the prefix \"Bearer \".")//
                .version("1.0.0")
                .license("")
                .licenseUrl("")
                .contact(new Contact("Spring Boot RESTful Web Service", null, "admin@gmail.com"))//
                .build();
    }
}
