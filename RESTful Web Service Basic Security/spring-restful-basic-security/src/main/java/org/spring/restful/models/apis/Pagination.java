package org.spring.restful.models.apis;


import lombok.Data;

@Data
public class Pagination {

    private int page;
    private int limit;
    private int totalCount;
    private int totalPages;

}
