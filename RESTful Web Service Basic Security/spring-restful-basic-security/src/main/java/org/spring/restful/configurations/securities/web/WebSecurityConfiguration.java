package org.spring.restful.configurations.securities.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Order(2)
@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    //Success Handler
    @Autowired
    private CustomSuccessHandler customSuccessHandler;

    //Failure Handler
    @Autowired
    private CustomFailureHandler customFailureHandler;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * TODO:
     * HasRole admin can access /admin/** and /e-banking
     * HasRole client can access /e-banking/**
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/swagger-ui.html**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                /*
                .formLogin().permitAll()
                .and()
                .logout().permitAll()
                */
                .formLogin()
                    .loginPage("/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .permitAll()
                    .successHandler(customSuccessHandler)
                    .failureHandler(customFailureHandler)
                .and()
                .exceptionHandling().accessDeniedPage("/errors/403");

    }

    /**
     * Create in memory users:
     * + User 1:
     * 	- Username: admin
     * 	- Password: 123456
     * 	- Roles: admin and client
     * + User 2:
     * 	- Username: client
     * 	- Password: 123456
     *  - Roles: client
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        String password = passwordEncoder().encode("123456");

        auth.inMemoryAuthentication().passwordEncoder(passwordEncoder())
                .withUser("admin").password(password).roles("ADMIN", "CLIENT")
                .and()
                .withUser("client").password(password).roles("CLIENT");

       /* auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());*/
    }




    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring().antMatchers("/resources/**")
                .and()
                .ignoring().antMatchers("/static/**");
    }
}
