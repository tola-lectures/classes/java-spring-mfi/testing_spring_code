package org.spring.restful.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfiguration implements WebMvcConfigurer {


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        // TODO: Access Denied page
        registry.addViewController("/errors/403").setViewName("/errors/403");
        registry.addViewController("/errors/401").setViewName("/errors/401");

        // TODO: Authentication page
        registry.addViewController("/login").setViewName("/authentications/login");

    }
}
