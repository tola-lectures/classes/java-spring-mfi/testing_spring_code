package org.spring.restful.models.bank;

import lombok.Getter;
import lombok.Setter;
import org.spring.restful.models.base.BaseEntity;
import org.spring.restful.models.company.Branch;
import org.spring.restful.models.customer.Customer;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Setter
@Getter
@Entity
public class Account extends BaseEntity {

    private String accountNumber;
    private long balance;
    private long interestRate;
    private String type;
    @ManyToOne
    private Branch branch;
    @ManyToOne
    private Customer customer;

}
