package org.spring.restful.models.forms;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Setter
@Getter
public class UserUpdateFrm {

    private long id;
    private String username;
    private String email;
    private String password;
    private boolean status;
    @JsonProperty("role_id")
    private List<Long> roleId;

}
