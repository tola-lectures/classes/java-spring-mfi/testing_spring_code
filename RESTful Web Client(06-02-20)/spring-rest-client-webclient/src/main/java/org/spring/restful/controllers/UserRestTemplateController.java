package org.spring.restful.controllers;

import org.spring.restful.models.User;
import org.spring.restful.models.forms.UserAddFrm;
import org.spring.restful.models.forms.UserUpdateFrm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/client/v1/user")
public class UserRestTemplateController {

    @Autowired
    private WebClient webClient;

    @GetMapping
    public Flux<Object> findAll() {
        return webClient.get()
                .uri("/user?page=0&size=10")
                .retrieve()
                .bodyToFlux(Object.class);
    }

    @GetMapping("/{id}")
    public Flux<Object> getById(@PathVariable("id") Long id) {
        return webClient.get()
                .uri("/user/"+id)
                .retrieve()
                .bodyToFlux(Object.class);
    }

    @PostMapping
    public Mono<Object> insert(@RequestBody UserAddFrm userAddFrm) {
        return webClient.post()
                .uri("/user")
                .body(Mono.just(userAddFrm), UserAddFrm.class)
                .retrieve()
                .bodyToMono(Object.class);
    }

    @PutMapping
    public Mono<Object> update(@RequestBody UserUpdateFrm userUpdateFrm) {
        return webClient.put()
                .uri("/user")
                .body(Mono.just(userUpdateFrm), UserUpdateFrm.class)
                .retrieve()
                .bodyToMono(Object.class);
    }

    @DeleteMapping("/{id}")
    public Flux<Object> delete(@PathVariable("id") Long id) {
        return webClient.delete()
                .uri("/user/"+ id)
                .retrieve()
                .bodyToFlux(Object.class);
    }


    /*@GetMapping
    public ResponseEntity<Object> findAll(
            @RequestParam(required = false, defaultValue = "0") int page, @RequestParam(required = false, defaultValue = "10") int size
    ){
        try{
            HttpEntity<Object> request = new HttpEntity<Object>(header);
            ResponseEntity<Object> response = rest.exchange( apiUrl+"/user?page="+page+"&size="+size, HttpMethod.GET , request , Object.class) ;
            System.out.println(response.getBody());
            return new ResponseEntity<Object>(response.getBody() , HttpStatus.OK);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") Long id){
        try{
            HttpEntity<Object> request = new HttpEntity<Object>(header);
            ResponseEntity<Object> response = rest.exchange( apiUrl+"/user/"+id, HttpMethod.GET , request , Object.class) ;
            return new ResponseEntity<Object>(response.getBody() , HttpStatus.OK);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping
    public ResponseEntity<Object> insert(@RequestBody UserAddFrm userAddFrm){
        HttpEntity<Object> request = new HttpEntity<Object>(userAddFrm, header);
        ResponseEntity<Object> response = rest.exchange(  apiUrl+"/user", HttpMethod.POST , request , Object.class) ;
        return new ResponseEntity<Object>(response.getBody() , HttpStatus.OK);
    }


    @PutMapping
    public ResponseEntity<Object> update(@RequestBody UserUpdateFrm userUpdateFrm){
        HttpEntity<Object> request = new HttpEntity<Object>(userUpdateFrm, header);
        ResponseEntity<Object> response = rest.exchange(  apiUrl+"/user", HttpMethod.PUT , request , Object.class) ;
        return new ResponseEntity<Object>(response.getBody() , HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable long id){
        HttpEntity<Object> request = new HttpEntity<Object>( header);
        ResponseEntity<Object> response = rest.exchange(  apiUrl+"/user/"+id, HttpMethod.PUT , request , Object.class) ;
        return new ResponseEntity<Object>(response.getBody() , HttpStatus.OK);
    }
*/

}
