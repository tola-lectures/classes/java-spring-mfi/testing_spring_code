package org.spring.restful.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@Getter
@Setter
public class User {

    private long id;
    private String username;
    private String email;
    private String password;
    List<Role> roleList;


}
