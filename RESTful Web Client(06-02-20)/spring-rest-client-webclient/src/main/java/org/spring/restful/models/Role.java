package org.spring.restful.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class Role {

    private long id;
    private String name;

}
