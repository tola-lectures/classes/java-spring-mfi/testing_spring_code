package org.spring.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestClientRestTemplatesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRestClientRestTemplatesApplication.class, args);
    }

}
