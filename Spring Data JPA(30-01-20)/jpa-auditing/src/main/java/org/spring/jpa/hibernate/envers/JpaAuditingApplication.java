package org.spring.jpa.hibernate.envers;

import org.spring.jpa.hibernate.envers.models.User;
import org.spring.jpa.hibernate.envers.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaAuditingApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(JpaAuditingApplication.class, args);
    }

    @Autowired
    private UserService userService;


    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setEmail("test@gmail.com");
        user.setPassword("123456");
        user.setStatus("1");
        user.setUsername("test");
        userService.save(user);




    }
}
