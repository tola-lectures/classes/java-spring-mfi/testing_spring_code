package org.spring.jpa.hibernate.envers.repositories;

import org.spring.jpa.hibernate.envers.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * @author tola on 1/30/20
 **/
@Repository
public interface UserRepository extends JpaRepository<User, Long> {




}
