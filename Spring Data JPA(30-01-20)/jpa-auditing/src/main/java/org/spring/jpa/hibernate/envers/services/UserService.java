package org.spring.jpa.hibernate.envers.services;

import org.spring.jpa.hibernate.envers.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author tola on 1/30/20
 **/
public interface UserService {

    User save(User user);

}
