package org.spring.migration.flyway;

import org.hibernate.envers.Audited;
import org.spring.migration.flyway.models.User;
import org.spring.migration.flyway.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMigrationFlywayApplication implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringMigrationFlywayApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        User u = new User();
        u.setEmail("test10@gmail.com");
        u.setGender("F");
        userRepository.save(u);

        User u1 = new User();
        u1.setId(1L);
        u1.setEmail("test20@gmail.com");
        userRepository.save(u1);


    }
}
