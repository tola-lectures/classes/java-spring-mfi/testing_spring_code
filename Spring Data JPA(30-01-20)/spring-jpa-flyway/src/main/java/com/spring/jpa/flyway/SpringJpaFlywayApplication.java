package com.spring.jpa.flyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaFlywayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJpaFlywayApplication.class, args);
    }

}
