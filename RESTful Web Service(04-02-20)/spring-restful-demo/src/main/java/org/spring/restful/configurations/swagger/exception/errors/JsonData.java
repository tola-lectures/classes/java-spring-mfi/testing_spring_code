package org.spring.restful.configurations.swagger.exception.errors;

/**
 * @author tola on 12/17/19
 **/
public class JsonData {

    private String keys;
    private String data;

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
