package org.spring.restful.models.company;

import lombok.Getter;
import lombok.Setter;
import org.spring.restful.models.base.BaseEntity;

import javax.persistence.Entity;

/**
 * @author tola on 1/29/20
 **/
@Getter
@Setter
@Entity
public class Branch extends BaseEntity {

    private String name;
    private String country;
    private String city;
    private String phone;
    private String address;

    /*@OneToMany
    @JoinColumn(name = "employee_id")
    private List<Employee> employees;*/


}
