package org.spring.restful.models.company;

import lombok.Getter;
import lombok.Setter;
import org.spring.restful.models.base.BaseEntity;

import javax.persistence.Entity;

@Setter
@Getter
@Entity
public class Department extends BaseEntity {

    private String name;

    /*@OneToMany
    @JoinColumn(name = "employee_id")
    private List<Employee> employees;

    @OneToMany
    @JoinColumn(name = "customer_id")
    private List<Customer> customer;*/


}
