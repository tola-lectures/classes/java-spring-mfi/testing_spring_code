package org.spring.restful.configurations.swagger;
/**
 *
 * @author tola on 9/10/19
 *
 **/
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Optional;


@Configuration
@EnableSwagger2
public class SpringFoxConfig {

    @Bean
    public Docket api() {

        //Adding Header
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder.name("Authorization")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue("Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiVVNFUl9DTElFTlRfUkVTT1VSQ0UiLCJVU0VSX0FETUlOX1JFU09VUkNFIl0sInVzZXJfbmFtZSI6ImFkbWluIiwic2NvcGUiOlsicm9sZV9hZG1pbiJdLCJpZCI6MSwiZXhwIjoyNDcyODM0NDAwLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5Ijoicm9sZV9hZG1pbiJ9LHsiYXV0aG9yaXR5IjoiY2FuX3JlYWRfdXNlciJ9LHsiYXV0aG9yaXR5IjoiY2FuX3VwZGF0ZV91c2VyIn0seyJhdXRob3JpdHkiOiJjYW5fY3JlYXRlX3VzZXIifSx7ImF1dGhvcml0eSI6ImNhbl9kZWxldGVfdXNlciJ9XSwianRpIjoiMTk1ZGQ0NjgtNjI0NC00MzIxLWFhOWMtZWMyNjE0NWFlZDQzIiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20iLCJjbGllbnRfaWQiOiJURVNUIiwidXNlcm5hbWUiOiJhZG1pbiJ9.sKhLcZCXFNYw8QA8FbWkVkuEGxq8GuxJnYFQ9yTeN4HbPFQvLUPUqXaxUf7LCCvIvcNG26yf6-NaUAYYA2vERwxOBHjkRI4WPVv2WJS3zTKue43nI3DKPRPVQpOVG83e0_EFrnUOrNgwJU66O9Nq1DxIMXkDpTkowuC4TqKBIzG-e7wALNvqnY5GlTdzBnJ63cCzcm3eDM70XA5ccgM8z7Wwb6q3RVHTOfb9kMD4zez_oMPL0pnp3mDe6cY_FbiOJ31YdbXXjM_VG_PAf7tfYWaSFgP0VG03hiqLzqD5M2vf0_xmc-KZlQk-RHrMl0IGvYejwfa4A6HfFiDIZqyQxg")        // based64 of - zone:mypassword
                .required(true)
                .build();
        java.util.List<Parameter> aParameters = new ArrayList<>();
        aParameters.add(aParameterBuilder.build());
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/v1/**"))
                .build()
                .apiInfo(metadata())
                .useDefaultResponseMessages(false)
                .globalOperationParameters(aParameters)
                .genericModelSubstitutes(Optional.class);
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Spring Boot RESTful Web Service")//
                .description("Spring Boot RESTful Web Service. You should click on the right top button `Authorize` and introduce it with the prefix \"Bearer \".")//
                .version("1.0.0")
                .license("")
                .licenseUrl("")
                .contact(new Contact("Spring Boot RESTful Web Service", null, "admin@gmail.com"))//
                .build();
    }
}
