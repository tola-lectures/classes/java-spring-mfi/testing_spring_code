package org.spring.restful.services.impl;

import org.spring.restful.models.user.Role;
import org.spring.restful.models.user.User;
import org.spring.restful.models.views.RoleView;
import org.spring.restful.models.views.UserView;
import org.spring.restful.repositories.UserRepository;
import org.spring.restful.services.UserServices;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserServices {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Page<UserView> findBy(Pageable pageable){
        return userRepository.findBy(pageable);
    }

    @Override
    public UserView findUserViewByUsername(String username) {
        return userRepository.findUserViewByUsername(username);
    }

    @Override
    public UserView findUserViewById(long id) {
        return userRepository.findUserViewById(id);
    }

    @Override
    public User insert(User user) {
        return userRepository.save(user);
    }

    @Override
    public UserView update(User userFrm) {
        UserView userView = userRepository.findUserViewById(userFrm.getId());
        User updateUser = null;
        if(userView != null){
            updateUser = new User();
            BeanUtils.copyProperties(userFrm , updateUser);
            User user = userRepository.save(updateUser);
            BeanUtils.copyProperties(user , userView);
        }
        return userView;
    }

    @Override
    public long delete(long id) {
        // findUserById
        // if != null
        // delete
        // else
        // return
        userRepository.deleteById(id);
        return id;
    }


}
