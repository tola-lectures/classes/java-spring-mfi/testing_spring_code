package org.spring.restful.models.views;

public interface RoleView {

    long getId();
    String getName();


}
