package org.spring.security.configurations.securities;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * TODO:
     * HasRole admin can access /admin/** and /e-banking
     * HasRole client can access /e-banking/**
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/home", "/about", "/contact").permitAll()
                .antMatchers("/e-banking/**").hasAnyRole("client", "admin")
                .antMatchers("/admin/**").hasRole("admin")
                .anyRequest().authenticated()
                .and()
                .formLogin().permitAll()
                .and()
                .logout().permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/errors/403");

    }

    /**
     * Create in memory users:
     * + User 1:
     * 	- Username: admin
     * 	- Password: 123456
     * 	- Roles: admin and client
     * + User 2:
     * 	- Username: client
     * 	- Password: 123456
     *  - Roles: client
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password("{noop}123456").roles("admin", "client")
                .and()
                .withUser("client").password("{noop}123456").roles("client");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**")
                .and()
                .ignoring().antMatchers("/static/**");
    }
}
