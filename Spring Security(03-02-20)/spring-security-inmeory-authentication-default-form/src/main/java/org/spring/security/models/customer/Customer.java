package org.spring.security.models.customer;

import lombok.Getter;
import lombok.Setter;
import org.spring.security.models.base.BaseEntity;
import org.spring.security.models.company.Branch;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class Customer extends BaseEntity {

    private String name;
    private String email;
    private String password;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date dob;
    @Column(length = 1)
    private String gender;
    private String position;
    private String phone;
    private String address;

    @ManyToOne
    private Branch branch;

   /* @OneToMany
    private List<File> file;*/



}
