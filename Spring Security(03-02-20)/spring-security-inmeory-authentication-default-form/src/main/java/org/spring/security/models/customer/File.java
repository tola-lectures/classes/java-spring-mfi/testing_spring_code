package org.spring.security.models.customer;

import lombok.Getter;
import lombok.Setter;
import org.spring.security.models.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class File extends BaseEntity {

    private String name;
    private long size;
    private String uploadType;
    @ManyToOne
    private Customer customer;


}
