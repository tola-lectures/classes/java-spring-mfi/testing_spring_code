package org.spring.security.services.users.impl;

import org.spring.security.models.user.User;
import org.spring.security.models.views.UserView;
import org.spring.security.repositories.UserRepository;
import org.spring.security.services.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public UserView findUserViewByUsername(String username) {
        return userRepository.findUserViewByUsername(username);
    }


}
