package org.spring.security.services.users;

import org.spring.security.models.user.Role;

public interface RoleService {

        Role save(Role role);

}
