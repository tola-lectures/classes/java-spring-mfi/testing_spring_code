package org.spring.security.models.bank;

import lombok.Getter;
import lombok.Setter;
import org.spring.security.models.base.BaseEntity;
import org.spring.security.models.company.Branch;
import org.spring.security.models.customer.Customer;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Setter
@Getter
@Entity
public class Account extends BaseEntity {

    private String accountNumber;
    private long balance;
    private long interestRate;
    private String type;
    @ManyToOne
    private Branch branch;
    @ManyToOne
    private Customer customer;

}
