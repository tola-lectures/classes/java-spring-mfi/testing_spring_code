package org.spring.security;

import org.spring.security.models.user.Role;
import org.spring.security.models.user.User;
import org.spring.security.repositories.RoleRepository;
import org.spring.security.services.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class SpringSecurityInmeoryAuthenticationApplication implements CommandLineRunner {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityInmeoryAuthenticationApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        // TODO : Insert two roles into database
        Role role1 = new Role();
        role1.setName("ROLE_ADMIN");
        Role role2 = new Role();
        role2.setName("ROLE_CLIENT");
        roleRepository.save(role1);
        roleRepository.save(role2);

        User user = new User();
        user.setUsername("admin");
        user.setEmail("admin@gmail.com");
        user.setPassword(new BCryptPasswordEncoder().encode("123456") );

        Role roleUser1 = new Role();
        roleUser1.setId(1);
        Role roleUser2 = new Role();
        roleUser2.setId(2);
        List<Role> roles = new ArrayList<>();
        roles.add(roleUser1);
        roles.add(roleUser2);

        user.setRoles(roles);

        User saved = userService.save(user);
        if(saved != null){
            System.out.println("****** Saved *********");
        }else {
            System.out.println("****** Error *********");
        }


        User userClient = new User();
        userClient.setUsername("client");
        userClient.setEmail("client@gmail.com");
        userClient.setPassword(new BCryptPasswordEncoder().encode("123456") );

        Role roleUserClient = new Role();
        roleUserClient.setId(2);
        roles = new ArrayList<>();
        roles.add(roleUserClient);

        userClient.setRoles(roles);

        User savedClient = userService.save(userClient);
        if(savedClient != null){
            System.out.println("****** Saved userClient *********");
        }else {
            System.out.println("****** Error userClient *********");
        }

        //User findByEmail = userService.findUserByUsername("admin");
        //System.out.println(findByEmail.toString());

    }
}
