package org.spring.security.models.user;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.spring.security.models.base.BaseEntity;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToMany(mappedBy = "roles")
    private List<User> users;// = new ArrayList<>();;


    @Override
    public String getAuthority() {
        return this.name;
    }
}
