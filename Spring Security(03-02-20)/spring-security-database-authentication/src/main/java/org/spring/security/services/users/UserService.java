package org.spring.security.services.users;

import org.spring.security.models.user.User;
import org.spring.security.models.views.UserView;

public interface UserService {

    User findUserByUsername(String username);
    User save(User user);

    UserView findUserViewByUsername(String username);




}
