package org.spring.security.services.users.impl;

import org.spring.security.models.user.Role;
import org.spring.security.models.user.User;
import org.spring.security.models.views.UserView;
import org.spring.security.services.users.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //User user = userService.findUserByUsername(username);

        UserView userView = userService.findUserViewByUsername(username);
        User user = null;
        if(userView != null){
            user = new User();
            BeanUtils.copyProperties(userView , user);
        }
        if(user == null){
            System.out.println("User not found!!!!!!");
            throw  new UsernameNotFoundException("User not found!!!!!");
        }else {
            System.out.println("User has found!!!!!!");
        }
        return  user;
    }
}
