package org.spring.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityInmeoryAuthenticationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityInmeoryAuthenticationApplication.class, args);
    }

}
