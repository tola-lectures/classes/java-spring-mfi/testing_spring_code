package org.spring.security.models.user;

import lombok.Getter;
import lombok.Setter;
import org.spring.security.models.base.BaseEntity;
import org.spring.security.models.company.Branch;
import org.spring.security.models.company.Department;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    private String name;

    private String email;

    private String password;

    @ManyToMany(mappedBy = "users")
    private List<Role> roles;



}
