package org.spring.security.models.user;

import lombok.Getter;
import lombok.Setter;
import org.spring.security.models.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

@Getter
@Setter
@Entity
public class Role extends BaseEntity {

    private String name;

    @ManyToMany
    @JoinTable(name = "user_roles",
        joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    private List<User> users;




}
