package org.spring.soap.ws.controllers;

import org.spring.soap.ws.models.soap.EmployeeDetailsRequest;
import org.spring.soap.ws.models.soap.EmployeeDetailsResponse;
import org.spring.soap.ws.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class EmployeeEndpoint {
	private static final String NAMESPACE_URI = "http://www.test.com/xml/school";

	@Autowired
	private EmployeeRepository employeeRepository;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "EmployeeDetailsRequest")
	@ResponsePayload
	public EmployeeDetailsResponse getEmployee(@RequestPayload EmployeeDetailsRequest request) {
		EmployeeDetailsResponse response = new EmployeeDetailsResponse();
		response.setEmployee(employeeRepository.findEmployee(request.getName()));
		return response;
	}
}