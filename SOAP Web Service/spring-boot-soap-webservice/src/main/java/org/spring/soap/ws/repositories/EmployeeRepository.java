package org.spring.soap.ws.repositories;

import org.spring.soap.ws.models.soap.Employee;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class EmployeeRepository {
    private static final Map<String, Employee> employeeHashMap = new HashMap<>();

    @PostConstruct
    public void initData() {

        Employee employee = new Employee();
        employee.setName("Dara");
        employee.setId(1);
        employee.setEmail("data@gmail.com");
        employeeHashMap.put(employee.getName(), employee);

        employee = new Employee();
        employee.setName("Sakada");
        employee.setId(2);
        employee.setEmail("sakada@gmail.com");
        employeeHashMap.put(employee.getName(), employee);

        employee = new Employee();
        employee.setName("Jojo");
        employee.setId(3);
        employee.setEmail("jojo@gmail.com");
        employeeHashMap.put(employee.getName(), employee);

        employee = new Employee();
        employee.setName("Kaka");
        employee.setId(4);
        employee.setEmail("kaka@gmail.com");
        employeeHashMap.put(employee.getName(), employee);


    }

    public Employee findEmployee(String name) {
        Assert.notNull(name, "The Employee's name must not be null");
        return employeeHashMap.get(name);
    }
}
