package org.spring.security.services.users.impl;

import org.spring.security.models.user.Role;
import org.spring.security.repositories.RoleRepository;
import org.spring.security.services.users.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository repository;

    @Override
    public Role save(Role role) {
        return repository.save(role);
    }
}
