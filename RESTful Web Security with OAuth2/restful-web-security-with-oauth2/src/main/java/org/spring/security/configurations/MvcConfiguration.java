package org.spring.security.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfiguration implements WebMvcConfigurer {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }



    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // TODO: Public Pages
        registry.addViewController("/").setViewName("/home");
        registry.addViewController("/home").setViewName("/home");
        registry.addViewController("/about").setViewName("/about");
        registry.addViewController("/contact").setViewName("/contact");

        // TODO: Client
        registry.addViewController("/e-banking").setViewName("/ebanking/ebanking");

        // TODO: Admin
        registry.addViewController("/admin").setViewName("/admin/admin");

        // TODO: Access Denied page
        registry.addViewController("/errors/403").setViewName("/errors/403");

        // TODO: Authentication page
        registry.addViewController("/login").setViewName("/authentications/login");

    }
}
