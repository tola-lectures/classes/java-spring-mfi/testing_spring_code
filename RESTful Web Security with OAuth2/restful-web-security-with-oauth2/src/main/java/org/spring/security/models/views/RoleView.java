package org.spring.security.models.views;

import org.springframework.security.core.GrantedAuthority;

public interface RoleView extends GrantedAuthority {

    long getId();
    String getName();


}
