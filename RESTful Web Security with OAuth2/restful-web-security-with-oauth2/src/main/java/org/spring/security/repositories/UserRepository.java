package org.spring.security.repositories;

import org.spring.security.models.user.User;
import org.spring.security.models.views.UserView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByUsername(String username);

    UserView findUserViewByUsername(String username);


}
