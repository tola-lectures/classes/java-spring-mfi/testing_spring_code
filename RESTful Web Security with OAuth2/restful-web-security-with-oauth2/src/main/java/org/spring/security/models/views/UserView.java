package org.spring.security.models.views;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserView extends UserDetailsService {

    long getId();
    @Value("#{target.username}")
    String getUsername();
    String getPassword();
    String getEmail();
    List<RoleView> getRoles();


}
