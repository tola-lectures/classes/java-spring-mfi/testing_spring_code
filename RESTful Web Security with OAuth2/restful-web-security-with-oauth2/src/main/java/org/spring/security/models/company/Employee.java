package org.spring.security.models.company;

import lombok.Getter;
import lombok.Setter;
import org.spring.security.models.base.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
public class Employee extends BaseEntity {

    private String name;

    private String email;

    private String password;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date dob;

    @Column(length = 1)
    private String gender;

    private String position;

    private String phone;

    private String addess;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @ManyToOne
    private Branch branch;

    @ManyToOne
    private Department department;

}
