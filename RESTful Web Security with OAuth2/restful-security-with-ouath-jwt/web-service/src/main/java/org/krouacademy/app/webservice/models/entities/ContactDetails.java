package org.krouacademy.app.webservice.models.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContactDetails {
    @Column(name = "email", nullable = false, length = 50, unique = true)
    private String email;

    @Column(name = "telephone", nullable = false, length = 20, unique = true)
    private String telephone;

    @Column(name = "mobile_phone", length = 20, unique = true)
    private String mobilePhone;

    @Column(name = "house_number")
    private String houseNumber;

    @Column(name = "street_number")
    private String streetNumber;

    @Column(name = "country")
    private String country;

    @Column(name = "province")
    private String province;

    @Column(name = "district")
    private String district;

    @Column(name = "commune")
    private String commune;

    @Column(name = "village")
    private String village;

    @Column(name = "postal_code")
    private String postalCode;
}
