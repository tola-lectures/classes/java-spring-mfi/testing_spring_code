package org.krouacademy.app.webservice.exceptions;

import com.google.gson.Gson;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class RestAuthenticationEntryPointConfiguration extends BasicAuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        response.setContentType("APPLICATION/JSON;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        try {
            Map<String, Object> httpResponse = new HashMap<>();
            httpResponse.put("code", "InvalidToken");
            httpResponse.put("timestamp", System.currentTimeMillis());
            httpResponse.put("message", "Http Status 401 - " + authException.getMessage());
            response.getWriter().write(new Gson().toJson(httpResponse));
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void afterPropertiesSet() {
        setRealmName("businessregistration.moc.gov.kh");
        super.afterPropertiesSet();
    }
}
