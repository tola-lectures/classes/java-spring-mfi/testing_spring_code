package org.krouacademy.app.webservice.service.impl;

import org.krouacademy.app.webservice.models.entities.Role;
import org.krouacademy.app.webservice.repositories.RoleRepository;
import org.krouacademy.app.webservice.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role save(Role role) {
        return roleRepository.save(role);
    }
}
