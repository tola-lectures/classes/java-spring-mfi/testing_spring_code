package org.krouacademy.app.webservice.service.impl;

import org.krouacademy.app.webservice.models.api.Pagination;
import org.krouacademy.app.webservice.models.entities.User;
import org.krouacademy.app.webservice.models.projection.UserView;
import org.krouacademy.app.webservice.repositories.UserRepository;
import org.krouacademy.app.webservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserView findUserViewByUsername(String username) {
        return userRepository.findUserViewByUsername(username);
    }

    @Override
    public List<UserView> findAllProjectedBy(Pagination pagination) {
        Page<UserView> userPage = userRepository.findUserViewBy(PageRequest.of(pagination.getPage(), pagination.getSize()));
        pagination.setTotalCounts(userPage.getTotalElements());
        return userPage.getContent();
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }


}
