package org.krouacademy.app.webservice.controllers.back;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.krouacademy.app.webservice.models.api.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/back/users")
@Slf4j
@AllArgsConstructor
public class UserBackRestController {

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public ApiResponse addNewUser(){
        return null;
    }

    @PutMapping("/id/{id}")
    public ApiResponse updateUser(@PathVariable Long id){
        return null;
    }

    @GetMapping("/id/{id}")
    public ApiResponse findUserById(@PathVariable Long id){
        return null;
    }

    @GetMapping
    public ApiResponse getAllUsers(){
        return null;
    }



}
