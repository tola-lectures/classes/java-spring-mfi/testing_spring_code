package org.krouacademy.app.webservice.models.projection;

import org.krouacademy.app.webservice.models.entities.enums.Status;

import java.util.List;

public interface UserView {

    Long getId();
    String getUsername();
    Status getStatus();
    List<RoleView> getRoles();

}
