package org.krouacademy.app.webservice.models.entities.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
public enum Title {
    MR("MR"),
    MRS("MRS"),
    MISS("MISS"),
    MS("MS"),
    DR("DR");

    private final String code;

    public static Title fromShortCode(String code) {
        switch (code) {
            case "MR":
                return Title.MR;
            case "MRS":
                return Title.MRS;
            case "MISS":
                return Title.MISS;
            case "MS":
                return Title.MS;
            case "DR":
                return Title.DR;
            default:
                throw new IllegalArgumentException("Code [" + code + "] not supported");
        }
    }

    private static List<Title> list() {
        List<Title> titles = new ArrayList<>();
        titles.add(MR);
        titles.add(MISS);
        titles.add(MRS);
        return titles;
    }

}
