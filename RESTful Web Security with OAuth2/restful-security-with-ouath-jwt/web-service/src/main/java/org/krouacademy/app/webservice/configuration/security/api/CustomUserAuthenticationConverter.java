package org.krouacademy.app.webservice.configuration.security.api;

import lombok.extern.slf4j.Slf4j;
import org.krouacademy.app.webservice.models.entities.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;

@Component
@Slf4j
public class CustomUserAuthenticationConverter implements UserAuthenticationConverter {

    private final String EMAIL = "email";

    private Collection<? extends GrantedAuthority> defaultAuthorities;

    public void setDefaultAuthorities(String[] defaultAuthorities) {
        this.defaultAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList(StringUtils.arrayToCommaDelimitedString(defaultAuthorities));
    }

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication userAuthentication) {
        Map<String, Object> response = new LinkedHashMap<String, Object>();
        response.put(USERNAME, userAuthentication.getName());

        if (userAuthentication.getAuthorities() != null && !userAuthentication.getAuthorities().isEmpty())
            response.put(AUTHORITIES, AuthorityUtils.authorityListToSet(userAuthentication.getAuthorities()));

        return response;
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {

        User user = new User();
        try{
            System.out.println("extractAuthentication  ====>  "+ map);
            user.setId(Long.valueOf(map.get("id").toString()));
            user.setEmail(map.get("email").toString());
            user.setUsername(map.get("username").toString());
        }catch(Exception e){ }

        if (map.containsKey(USERNAME))
            return new UsernamePasswordAuthenticationToken(
                    user, "N/A",
                    getAuthorities(map));
        return null;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Map<String, ?> map) {

        Object authorities1 = map.get(AUTHORITIES);

        System.out.println(map.get(AUTHORITIES));
        System.out.println("map  " + map);

        List<Map<String, Object>> mapList = (List<Map<String, Object>>) map.get(AUTHORITIES);
        System.out.println("authorities1  " + mapList.get(0).get("authority"));

        if(mapList != null){
            Set<GrantedAuthority> authorities = new HashSet<>();
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority((String) mapList.get(0).get("authority"));
            authorities.add(grantedAuthority);
            return authorities;
        }


        if (!map.containsKey(AUTHORITIES))
            return defaultAuthorities;

        Object authorities = map.get(AUTHORITIES);

        if (authorities instanceof String)
            return AuthorityUtils.commaSeparatedStringToAuthorityList((String) authorities);

        if (authorities instanceof Collection)
            return AuthorityUtils.commaSeparatedStringToAuthorityList(
                    StringUtils.collectionToCommaDelimitedString((Collection<?>) authorities));

        throw new IllegalArgumentException("Authorities must be either a String or a Collection");
    }

}
