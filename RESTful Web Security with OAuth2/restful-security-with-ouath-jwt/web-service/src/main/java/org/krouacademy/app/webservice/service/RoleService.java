package org.krouacademy.app.webservice.service;


import org.krouacademy.app.webservice.models.entities.Role;

public interface RoleService {

    Role save(Role role);

}
