package org.krouacademy.app.webservice.exceptions;

import com.google.gson.Gson;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class AccessDeniedHandlerConfiguration implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        httpServletResponse.setContentType("APPLICATION/JSON;charset=UTF-8");
        httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Map<String, Object> httpResponse = new HashMap<>();
            httpResponse.put("timestamp", System.currentTimeMillis());
            if (auth != null) {
                httpResponse.put("message", "User: " + auth.getName() + " attempted to access the protected URL: " + httpServletRequest.getRequestURI());
            }else {
                httpResponse.put("message", "Http Status 403 - " + e.getMessage());
            }

            httpServletResponse.getWriter().write(new Gson().toJson(httpResponse));
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
