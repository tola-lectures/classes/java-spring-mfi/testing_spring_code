package org.krouacademy.app.webservice.controllers;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.krouacademy.app.webservice.models.api.ApiResponse;
import org.krouacademy.app.webservice.models.api.Pagination;
import org.krouacademy.app.webservice.models.entities.User;
import org.krouacademy.app.webservice.models.projection.UserView;
import org.krouacademy.app.webservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@RequestMapping("/api/v1/front/users")
@Slf4j
@AllArgsConstructor
public class UserRestController {

    @Autowired
    private UserService userService;

    /**
     * To Find All Officers
     * @param pagination Pagination
     * @return ApiResponse List of Users
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", defaultValue = "0",
                    value = "Result page you want to retrieve 1..N"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", defaultValue = "15",
                    value = "Number of records per page")
    })
    @GetMapping
    public ApiResponse<List<UserView>> findAllUsers(@ApiIgnore Pagination pagination) {
        List<UserView> userViews = userService.findAllProjectedBy(pagination);
        return new ApiResponse<>("OK", "User has been fetched successfully", userViews);
    }

    @GetMapping("/username/{username}")
    public ApiResponse<UserView> findUserByUsername(@PathVariable String username) {
        UserView userView = userService.findUserViewByUsername(username);
        return new ApiResponse<>("OK", "User has been fetched successfully", userView);
    }

    @PostMapping
    public ApiResponse<UserView> save(@RequestBody User user) {
        //UserView userView = userService.findUserViewByUsername(username);
        return new ApiResponse<>("OK", "User has been fetched successfully");
    }

}
