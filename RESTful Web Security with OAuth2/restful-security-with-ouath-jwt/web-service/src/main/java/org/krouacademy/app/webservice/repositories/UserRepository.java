package org.krouacademy.app.webservice.repositories;

import org.krouacademy.app.webservice.models.entities.User;
import org.krouacademy.app.webservice.models.projection.UserView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findUserByUsername(String username);

    // Projection
    UserView findUserViewByUsername(String username);
    Page<UserView> findUserViewBy(Pageable pageable);



}
