package org.krouacademy.app.webservice.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.krouacademy.app.webservice.models.entities.enums.Status;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Role implements GrantedAuthority  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable=false, updatable=false, nullable = false)
    private Long id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "status", nullable = false)
    private Status status;

    /*@ManyToMany(mappedBy = "roles")
    private List<User> users;*/

    @Override
    public String getAuthority() {
        return this.name;
    }

}
