package org.krouacademy.app.webservice.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.krouacademy.app.webservice.models.entities.enums.Gender;
import org.krouacademy.app.webservice.models.entities.enums.Title;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person extends BaseEntityAudit {

    @Column(name = "lastname_kh", nullable = false, length = 50)
    private String lastName;

    @Column(name = "lastname_en", nullable = false, length = 50)
    private String lastNameEn;

    @Column(name = "firstname_kh", nullable = false, length = 50)
    private String firstName;

    @Column(name = "firstname_en", nullable = false, length = 50)
    private String firstNameEn;

    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(name = "place_of_birth", nullable = false, length = 200)
    private String placeOfBirth;

    @Column(name = "gender", nullable = false, length = 1)
    private Gender gender;

    @Column(name = "title", nullable = false, length = 4)
    @Enumerated(EnumType.STRING)
    private Title title;

    @Embedded
    private ContactDetails contactDetails;


}
