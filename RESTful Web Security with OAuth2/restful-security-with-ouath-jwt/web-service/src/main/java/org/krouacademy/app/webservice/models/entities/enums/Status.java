package org.krouacademy.app.webservice.models.entities.enums;

public enum Status {
    INACTIVE,
    ACTIVATED
}
