package org.krouacademy.app.webservice;

import org.krouacademy.app.webservice.models.entities.Role;
import org.krouacademy.app.webservice.models.entities.User;
import org.krouacademy.app.webservice.models.entities.enums.Status;
import org.krouacademy.app.webservice.service.RoleService;
import org.krouacademy.app.webservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class WebServiceApplication implements CommandLineRunner {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(WebServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        // TODO : Insert two roles into database
        Role role1 = new Role();
        role1.setName("ROLE_ADMIN");
        role1.setStatus(Status.ACTIVATED);
        Role role2 = new Role();
        role2.setName("ROLE_CLIENT");
        role2.setStatus(Status.ACTIVATED);
        roleService.save(role1);
        roleService.save(role2);

        User user = new User();
        user.setUsername("admin");
        user.setEmail("admin@gmail.com");
        user.setPassword(passwordEncoder.encode("123456"));
        user.setStatus(Status.ACTIVATED);

        Role roleUser1 = new Role();
        roleUser1.setId(1L);
        Role roleUser2 = new Role();
        roleUser2.setId(2L);
        List<Role> roles = new ArrayList<>();
        roles.add(roleUser1);
        roles.add(roleUser2);

        user.setRoles(roles);

        User saved = userService.save(user);
        if(saved != null){
            System.out.println("****** Saved *********");
        }else {
            System.out.println("****** Error *********");
        }


        User userClient = new User();
        userClient.setUsername("client");
        userClient.setEmail("client@gmail.com");
        userClient.setPassword(passwordEncoder.encode("123456"));
        userClient.setStatus(Status.ACTIVATED);

        Role roleUserClient = new Role();
        roleUserClient.setId(2L);
        roles = new ArrayList<>();
        roles.add(roleUserClient);

        userClient.setRoles(roles);

        User savedClient = userService.save(userClient);
        if(savedClient != null){
            System.out.println("****** Saved userClient *********");
        }else {
            System.out.println("****** Error userClient *********");
        }

    }
}
