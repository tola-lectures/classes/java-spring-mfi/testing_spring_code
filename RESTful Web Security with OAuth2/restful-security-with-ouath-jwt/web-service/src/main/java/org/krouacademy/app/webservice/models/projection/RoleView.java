package org.krouacademy.app.webservice.models.projection;

public interface RoleView {
    Long getId();
    String getName();
}
