package org.krouacademy.app.webservice.service;

import org.krouacademy.app.webservice.models.api.Pagination;
import org.krouacademy.app.webservice.models.entities.User;
import org.krouacademy.app.webservice.models.projection.UserView;

import java.util.List;

public interface UserService {

    UserView findUserViewByUsername(String username);
    List<UserView> findAllProjectedBy(Pagination pagination);
    User save(User user);


}
