package org.krouacademy.app.webservice.configuration.security.api;

import io.micrometer.core.instrument.util.IOUtils;
import org.krouacademy.app.webservice.exceptions.AccessDeniedHandlerConfiguration;
import org.krouacademy.app.webservice.exceptions.RestAuthenticationEntryPointConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.io.IOException;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfiguration extends org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter {

    @Autowired
    private RestAuthenticationEntryPointConfiguration authenticationEntryPointConfiguration;

    @Autowired
    private AccessDeniedHandlerConfiguration accessDeniedConfiguration;

    @Autowired
    private CustomAccessTokenConverter customAccessTokenConverter;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.exceptionHandling()
                .accessDeniedHandler(accessDeniedConfiguration)
                .authenticationEntryPoint(authenticationEntryPointConfiguration);

        http.csrf().disable().antMatcher("/**");

        http.cors();

        http.authorizeRequests().antMatchers("/api/v1/front/**").authenticated();

        http.authorizeRequests().antMatchers("/api/v1/back/**").authenticated();



//      http.authorizeRequests().anyRequest().authenticated();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        return defaultTokenServices;
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        Resource resource = new ClassPathResource("jwt-public.pem");
        String publicKey = null;
        try {
            publicKey = IOUtils.toString(resource.getInputStream());
        } catch (final IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        converter.setAccessTokenConverter(customAccessTokenConverter);
        converter.setVerifierKey(publicKey);
        return converter;
    }


}
