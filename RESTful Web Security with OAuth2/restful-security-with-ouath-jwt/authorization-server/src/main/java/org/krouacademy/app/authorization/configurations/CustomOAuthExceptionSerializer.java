package org.krouacademy.app.authorization.configurations;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Map;

public class CustomOAuthExceptionSerializer extends StdSerializer<CustomOAuthException> {
    public CustomOAuthExceptionSerializer() {
        super(CustomOAuthException.class);
    }

    @Override
    public void serialize(CustomOAuthException value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        if(value.getHttpErrorCode() == 400){
            if(value.getMessage().equals("Bad credentials")){
                jsonGenerator.writeNumberField("code", value.getHttpErrorCode());
                jsonGenerator.writeStringField("message", "Email or Password is invalid");
            }else{
                jsonGenerator.writeStringField("message", value.getMessage());
                jsonGenerator.writeStringField("code", value.getOAuth2ErrorCode());
            }
        }else {
            jsonGenerator.writeStringField("message", value.getMessage());
        }
        if (value.getAdditionalInformation()!=null) {
            for (Map.Entry<String, String> entry : value.getAdditionalInformation().entrySet()) {
                String key = entry.getKey();
                String add = entry.getValue();
                jsonGenerator.writeStringField(key, add);
            }
        }
        jsonGenerator.writeEndObject();
    }
}
