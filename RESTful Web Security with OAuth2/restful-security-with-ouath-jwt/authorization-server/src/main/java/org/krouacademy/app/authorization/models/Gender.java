package org.krouacademy.app.authorization.models;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public enum Gender  {
    MALE("M"),
    FEMALE("F");

    private final String code;

    public static Gender fromShortCode(String code) {
        switch (code) {
            case "M":
                return Gender.MALE;
            case "F":
                return Gender.FEMALE;
            default:
                throw new IllegalArgumentException("Code [" + code + "] not supported");
        }
    }

    public static List<Gender> list() {
        List<Gender> genders = new ArrayList<>();
        genders.add(MALE);
        genders.add(FEMALE);
        return genders;
    }
}
