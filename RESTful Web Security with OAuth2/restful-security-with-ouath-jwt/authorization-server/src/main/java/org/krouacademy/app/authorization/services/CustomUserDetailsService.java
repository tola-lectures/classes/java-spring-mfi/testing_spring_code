package org.krouacademy.app.authorization.services;

import org.krouacademy.app.authorization.models.User;
import org.krouacademy.app.authorization.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("======== loadUserByUsername ======" );
        User user = userRepository.findUserByUsername(username);
        System.out.println("========" + user.toString());
        if (user != null) {
            new AccountStatusUserDetailsChecker().check(user);
            return user;
        }
        throw new UsernameNotFoundException("User " + username + " was not found in our system");
    }
}
