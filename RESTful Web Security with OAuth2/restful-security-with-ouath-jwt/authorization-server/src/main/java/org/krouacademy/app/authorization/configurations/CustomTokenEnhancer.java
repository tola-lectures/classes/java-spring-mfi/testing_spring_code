package org.krouacademy.app.authorization.configurations;

/**
 * @author tola on 10/28/19
 **/

import org.krouacademy.app.authorization.models.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.LinkedHashMap;
import java.util.Map;

public class CustomTokenEnhancer extends JwtAccessTokenConverter {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        System.out.println("****** CustomTokenEnhancer ");

        if(authentication.getPrincipal() == null){
            System.out.println("****** getPrincipal == null ");
        }else{
            System.out.println("****** getPrincipal  " + authentication.getPrincipal());
        }

        Map<String, Object> info = new LinkedHashMap<String, Object>(accessToken.getAdditionalInformation());
        try{
           if(authentication.getPrincipal() != null) {
               User user = (User) authentication.getPrincipal();
               info.put("email", user.getEmail());
               info.put("username", user.getUsername());
               info.put("id", user.getId());
               info.put("authorities", user.getAuthorities());
               info.put("role_name", user.getRoles().get(0).getName());
           }
        }catch (Exception e){
            e.printStackTrace();
        }

        DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
        customAccessToken.setAdditionalInformation(info);
        System.out.println("****** return ");
        return super.enhance(customAccessToken, authentication);
    }
}
