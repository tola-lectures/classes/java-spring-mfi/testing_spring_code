package org.krouacademy.app.authorization.models;

public enum Status {
    INACTIVE,
    ACTIVATED
}
