package org.krouacademy.app.authorization.repositories;

import org.krouacademy.app.authorization.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author tola on 10/28/19
 **/
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByUsername(String username);
}
