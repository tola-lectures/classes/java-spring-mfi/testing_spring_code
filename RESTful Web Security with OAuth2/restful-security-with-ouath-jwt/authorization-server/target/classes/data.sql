INSERT INTO OAUTH_CLIENT_DETAILS (
	client_id,client_secret,
	resource_ids,
	scope,
	authorized_grant_types,
	web_server_redirect_uri,authorities,
	access_token_validity,refresh_token_validity,
	additional_information,autoapprove)
VALUES(
    -- password = 123456
    'TESTAPP','{bcrypt}$2a$10$MliFtxi6vKM/cl.90QeVruv0ADujMGH.vaM2ak3xyX182kpCE312K',
	'USER_CLIENT_RESOURCE,USER_ADMIN_RESOURCE',
	'read,write',
	'authorization_code,password,refresh_token,implicit,client_credentials',
	NULL,NULL,
	900,900000000,
	'{}',NULL
),(
    'TESTAPP2','{bcrypt}$2a$10$MliFtxi6vKM/cl.90QeVruv0ADujMGH.vaM2ak3xyX182kpCE312K',
	'USER_CLIENT_RESOURCE,USER_ADMIN_RESOURCE',
	'read,write',
	'authorization_code,password,refresh_token,implicit,client_credentials',
	NULL,NULL,
	900000000,900000000,
	'{}',NULL
);

