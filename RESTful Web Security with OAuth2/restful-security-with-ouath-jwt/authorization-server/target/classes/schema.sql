--DROP TABLE IF EXISTS roles_permissions;
--DROP TABLE IF EXISTS users_roles;
--DROP TABLE IF EXISTS permissions;
--DROP TABLE IF EXISTS roles;
--DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS OAUTH_CLIENT_DETAILS;
DROP TABLE IF EXISTS oauth_approvals;
DROP TABLE IF EXISTS oauth_code;
DROP TABLE IF EXISTS oauth_refresh_token;
DROP TABLE IF EXISTS oauth_access_token;


create  table if not exists oauth_client_details (
    client_id varchar(255) not null primary key,
    client_secret varchar(255) not null,
    resource_ids varchar(255) default null,
    scope varchar(255) default null,
    authorized_grant_types varchar(255) default null,
    web_server_redirect_uri varchar(255) default null,
    authorities varchar(255) default null,
    access_token_validity integer default null,
    refresh_token_validity integer default null,
    additional_information varchar(4096) default null,
    autoapprove varchar(255) default null
);


--create table permissions (id  bigserial not null, name varchar(255), primary key (id));
--create table roles (id  bigserial not null, created_date TIMESTAMP default NOW(), name varchar(255), status varchar(100), primary key (id));
--create table roles_permissions (role_id int8 not null, permissions_id int8 not null);
--create table users (id  bigserial not null, account_expired int4, account_locked int4 default 1, created_date TIMESTAMP default NOW(), credentials_expired int4, email varchar(255), enabled int4 not null, password varchar(255), status varchar(100) default '1', username varchar(255), primary key (id));
--create table users_roles (user_id int8 not null, roles_id int8 not null);



/*
CREATE TABLE IF NOT EXISTS PERMISSIONS (
    ID bigserial NOT NULL PRIMARY KEY,
    NAME VARCHAR(60) UNIQUE
);


CREATE TABLE IF NOT EXISTS ROLES (
    ID bigserial NOT NULL PRIMARY KEY,
    NAME VARCHAR(60) UNIQUE,
    created_date  TIMESTAMP default NOW(),
    status VARCHAR(1) default '1'
);


CREATE TABLE IF NOT EXISTS PERMISSION_ROLES(
    PERMISSION_ID int8,
    FOREIGN KEY(PERMISSION_ID) REFERENCES PERMISSIONS(ID),
    ROLE_ID int8,
    FOREIGN KEY(ROLE_ID) REFERENCES ROLES(ID)
);


CREATE TABLE IF NOT EXISTS  "users" (
    ID bigserial NOT NULL PRIMARY KEY,
    USERNAME VARCHAR(24) UNIQUE NOT NULL,
    PASSWORD VARCHAR(255) NOT NULL,
    EMAIL VARCHAR(255) NOT NULL,
    ENABLED INTEGER NOT NULL,
    ACCOUNT_EXPIRED INTEGER NOT NULL,
    CREDENTIALS_EXPIRED INTEGER NOT NULL,
    ACCOUNT_LOCKED INTEGER NOT NULL,
    created_date  TIMESTAMP default NOW(),
    status VARCHAR(1) default '1'
);



CREATE TABLE IF NOT EXISTS users_roles (
    ROLE_ID int8,FOREIGN KEY(ROLE_ID) REFERENCES ROLES(ID),
    USER_ID int8, FOREIGN KEY(USER_ID) REFERENCES "users"(ID)
);

*/


create table IF NOT EXISTS oauth_access_token(
	authentication_id varchar(256) PRIMARY KEY,
	token_id varchar(256),
	token BYTEA,
	user_name varchar(256),
	client_id varchar(256),
	authentication BYTEA,
	refresh_token varchar(256)
);

create table IF NOT EXISTS oauth_refresh_token(
		token_id varchar(256),
		token bytea,
		authentication bytea
);

create table if not exists oauth_code (
  code VARCHAR(255), authentication bytea
);

create table if not exists oauth_approvals (
	userId VARCHAR(255),
	clientId VARCHAR(255),
	scope VARCHAR(255),
	status VARCHAR(10),
	expiresAt TIMESTAMP,
	lastModifiedAt TIMESTAMP
);


