package org.spring.soap.ws;

import org.spring.soap.ws.configurations.SOAPConnector;
import org.spring.soap.ws.model.soap.EmployeeDetailsRequest;
import org.spring.soap.ws.model.soap.EmployeeDetailsResponse;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootSoapClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSoapClientApplication.class, args);
	}
	
	@Bean
	CommandLineRunner lookup(SOAPConnector soapConnector) {
		return args -> {
			String name = "Dara";//Default Name
			EmployeeDetailsRequest request = new EmployeeDetailsRequest();
			request.setName(name);
			EmployeeDetailsResponse response =(EmployeeDetailsResponse) soapConnector.callWebService("http://localhost:8080/service/student-details", request);
			System.out.println("Got Response As below ========= : ");
			System.out.println("Name : "+response.getEmployee().getName());
			System.out.println("Id : "+response.getEmployee().getId());
			System.out.println("Email : "+response.getEmployee().getEmail());
		};
	}
}
