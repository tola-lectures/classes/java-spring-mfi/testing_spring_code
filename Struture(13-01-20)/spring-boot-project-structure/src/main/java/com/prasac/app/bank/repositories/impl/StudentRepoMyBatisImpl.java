package com.prasac.app.bank.repositories.impl;

import com.prasac.app.bank.models.Student;
import com.prasac.app.bank.repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;

//import com.prasac.app.bank.utilities.DBConnection;

@Repository//("studentRepoBean")
public class StudentRepoMyBatisImpl implements StudentRepo {

    private Connection cnn;
    private String sql;
    private Statement stmt;
    private ResultSet rs;
    private PreparedStatement pstmt;

    //@Autowired
    private DataSource dataSource;

    @Autowired
    public StudentRepoMyBatisImpl(DataSource dataSource) throws SQLException {
        //cnn = DBConnection.initDB();
        cnn = dataSource.getConnection();
    }

    @Override
    public boolean insertStudent(Student student) {
        sql = "INSERT INTO student values(" +
                "'"+ student.getId() + "',"+
                "'"+student.getFullName() + "' , "+
                "'"+student.getGender() + "' , "+
                "'"+student.getMajor() + "' , "+
                "'"+student.getDob() +"')" ;
        System.out.println(sql);
        try{
            stmt = cnn.createStatement();
            if(stmt.executeUpdate(sql) != 0){
                //System.out.println("Data has been inserted");
                return  true;
            }else{
                //System.out.println("Data has not been inserted");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updateStudentById(Student student) {
        return false;
    }

    @Override
    public boolean deletedStuentById(int id) {
        return false;
    }

    @Override
    public ArrayList<Student> getAllStudents() {
        sql = "Select * from student";
        ArrayList<Student> students = new ArrayList<>();
        Student student = null;
        try{
            stmt = cnn.createStatement();
            rs = stmt.executeQuery(sql);
            while(rs.next()){
                /*student = new Student(
                        rs.getInt("id"),
                        rs.getString("full_name"),
                        rs.getString("gender"),
                        rs.getString("major"),
                        rs.getString("dob")
                );*/
                students.add(new Student(
                        rs.getInt("id"),
                        rs.getString("full_name"),
                        rs.getString("gender"),
                        rs.getString("major"),
                        rs.getString("dob")
                ));
            }
        }catch ( SQLException e){
            e.printStackTrace();
        }
        return students;
    }

    @Override
    public Student findStudentById(int id) {
        sql = "Select * from student where id=?";
        Student student = null;
        try{
            pstmt = cnn.prepareStatement(sql);
            pstmt.setInt(1,id);
            rs = pstmt.executeQuery();
            if(rs.next()){
                student = new Student(
                        rs.getInt("id"),
                        rs.getString("full_name"),
                        rs.getString("gender"),
                        rs.getString("major"),
                        rs.getString("dob")
                );
            }
        }catch ( SQLException e){
            e.printStackTrace();
        }
        return student;
    }
}
