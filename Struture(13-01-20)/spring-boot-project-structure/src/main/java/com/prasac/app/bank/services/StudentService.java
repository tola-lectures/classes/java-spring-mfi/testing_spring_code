package com.prasac.app.bank.services;

import com.prasac.app.bank.models.Student;

import java.util.ArrayList;

public interface StudentService {

    boolean insertStudent(Student student);
    boolean updateStudentById(Student student);
    boolean deletedStuentById(int id);
    ArrayList<Student> getAllStudents();
    Student findStudentById(int id);
}
