package com.prasac.app.bank.services.impl;

import com.prasac.app.bank.models.Student;
import com.prasac.app.bank.repositories.StudentRepo;
import com.prasac.app.bank.repositories.impl.StudentRepoImpl;
import com.prasac.app.bank.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    @Qualifier("studentRepoBean")
    private StudentRepo studentRepo;

    //@Autowired
    //private StudentRepoImpl studentRepImpl;

    @Override
    public boolean insertStudent(Student student) {
        if(student.getMajor().equalsIgnoreCase("IT")){
            return false;
        }else if(student.getMajor().equalsIgnoreCase("ACCOUNOTING")){
            return  false;
        }else{
            studentRepo.insertStudent(student);
        }
        return false;
    }

    @Override
    public boolean updateStudentById(Student student) {
        return studentRepo.updateStudentById(student);
    }

    @Override
    public boolean deletedStuentById(int id) {
        return studentRepo.deletedStuentById(id);
    }

    @Override
    public ArrayList<Student> getAllStudents() {
        return studentRepo.getAllStudents();
    }

    @Override
    public Student findStudentById(int id) {
        return studentRepo.findStudentById(id);
    }
}
