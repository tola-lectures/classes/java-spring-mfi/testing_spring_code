package com.prasac.app.bank;

import com.prasac.app.bank.repositories.StudentRepo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
/*
@ComponentScan(
        {
               // "com.prasac.app.bank"
                //"com.prasac.app.bank.controllers"
        })

@EnableAutoConfiguration
@Configuration*/
public class TestSpringBootMavenApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestSpringBootMavenApplication.class, args);
        //ConfigurableApplicationContext context = SpringApplication.run(TestSpringBootMavenApplication.class, args);
        //StudentRepo studentRepo = (StudentRepo) context.getBean("studentRepoImpl");
        //System.out.println(studentRepo.findStudentById(1).toString());
    }

}
