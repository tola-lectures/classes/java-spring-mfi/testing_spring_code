package com.prasac.app.bank.controllers;

import com.prasac.app.bank.repositories.StudentRepo;
import com.prasac.app.bank.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("prasac")
public class PageController {

    // TODO CONSTRUCTOR INJECTION
    /*
    private StudentRepo studentRepo;
    @Autowired
    public PageController(*//*@Qualifier("studentRepoImpl") *//*StudentRepo studentRepo){
        this.studentRepo = studentRepo;
    }
    */

    // TODO Setter Injection
    /*
    private StudentRepo studentRepo;
    @Autowired
    public void setStudentRepo(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }
    */

    // TODO FIELD INJECTION
    @Autowired
    //@Qualifier("studentRepoImpl")
    private StudentService studentService;


    @RequestMapping({"/index", "/" , "/home"})
    public String indexPage(ModelMap modelMap){
        System.out.println("Index page");
        modelMap.addAttribute("data",studentService.findStudentById(1));
        return "index";
    }

    @RequestMapping("/get")
    @ResponseBody
    public String getAllStudents(){
        return studentService.getAllStudents().toString();
    }


}
